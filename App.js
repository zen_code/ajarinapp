import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  FlatList,
  AsyncStorage,
  ActivityIndicator,
  Platform
} from "react-native";
import {
  createStackNavigator,
  createDrawerNavigator,
  createAppContainer,
  NavigationActions,
  NavigationAction
} from "react-navigation";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Item,
  Card,
  CardItem,
  Input,
  Thumbnail,
  Fab,
  Icon
} from "native-base";

import SplashScreen from "./src/app/screens/SplashScreen";
import MainMenu from "./src/app/screens/MainMenu";
import LoginTentor from "./src/app/screens/LoginTentor";
import LoginStudent from "./src/app/screens/LoginStudent";
import RegisterTentor from "./src/app/screens/RegisterTentor";
import RegisterStudent from "./src/app/screens/RegisterStudent";
import HomeMenuStudent from "./src/app/screens/HomeMenuStudent";
import HomeMenuTentor from "./src/app/screens/HomeMenuTentor";
import Artikel from "./src/app/screens/Artikel/Artikel";
import AllTentor from "./src/app/screens/Tentor/AllTentor";
import DetailTentor from "./src/app/screens/Tentor/DetailTentor";

import History from "./src/app/screens/History/History";
import Jadwal from "./src/app/screens/Jadwal/Jadwal";


import ScanToko from "./src/app/screens/ScanToko";
import DetailProduk from "./src/app/screens/Produk/DetailProduk";
import AllProduk from "./src/app/screens/Produk/AllProduk";
import AllShop from "./src/app/screens/Produk/AllShop";
import FilterProduk from "./src/app/screens/Produk/FilterProduk";
import ChatStore from "./src/app/screens/Chat/ChatStore";
import ChatView from "./src/app/screens/Chat/ChatView";
import Favorite from "./src/app/screens/Favorite/Favorite";
import Troli from "./src/app/screens/Keranjang/Troli";
import Account from "./src/app/screens/Akun/Account";
import SettingAccount from "./src/app/screens/Akun/SettingAccount";
import MyShop from "./src/app/screens/Toko/MyShop";
import AktivasiMyShop from "./src/app/screens/Toko/AktivasiMyShop";
import AddProduct from "./src/app/screens/Toko/AddProduct";
import UpdateProduct from "./src/app/screens/Toko/UpdateProduct";
import MyProduct from "./src/app/screens/Toko/MyProduct";
import DetailProdukShop from "./src/app/screens/Toko/DetailProdukShop";
import DetailShop from "./src/app/screens/Produk/DetailShop";
import FormOrder from "./src/app/screens/Produk/FormOrder";
import ToPay from "./src/app/screens/Akun/ToPay";
import ToShip from "./src/app/screens/Akun/ToShip";
import Receive from "./src/app/screens/Akun/Receive";
import Completed from "./src/app/screens/Akun/Completed";
import Cancelled from "./src/app/screens/Akun/Cancelled";
import Return from "./src/app/screens/Akun/Return";
import SUnpaid from "./src/app/screens/Toko/SUnpaid";
import SToShip from "./src/app/screens/Toko/SToShip";
import SShipping from "./src/app/screens/Toko/SShipping";
import SCompleted from "./src/app/screens/Toko/SCompleted";
import SCancelled from "./src/app/screens/Toko/SCancelled";
import SReturn from "./src/app/screens/Toko/SReturn";
import Investor from "./src/app/screens/Toko/Investor";

import ChatHeader from "./src/app/screens/Chat/ChatHeader";
import Chat from "./src/app/screens/Chat/Chat";

const NavStack = createStackNavigator({
  SplashScreen: {
    screen: SplashScreen
  },
  MainMenu: {
    screen: MainMenu
  },
  LoginTentor: {
    screen: LoginTentor
  },
  LoginStudent: {
    screen: LoginStudent
  },
  RegisterTentor: {
    screen: RegisterTentor
  },
  RegisterStudent: {
    screen: RegisterStudent
  },
  HomeMenuStudent: {
    screen: HomeMenuStudent
  },
  HomeMenuTentor: {
    screen: HomeMenuTentor
  },
  Artikel: {
    screen: Artikel
  },
  AllTentor: {
    screen: AllTentor
  },
  DetailTentor: {
    screen: DetailTentor
  },
  History: {
    screen: History
  },
  Jadwal: {
    screen: Jadwal
  },


  ScanToko: {
    screen: ScanToko
  },
  AllProduk: {
    screen: AllProduk
  },
  AllShop: {
    screen: AllShop
  },
  FilterProduk: {
    screen: FilterProduk
  },
  DetailProduk: {
    screen: DetailProduk
  },
  ChatStore: {
    screen: ChatStore
  },
  ChatView: {
    screen: ChatView
  },
  Favorite: {
    screen: Favorite
  },
  Troli: {
    screen: Troli
  },
  Account: {
    screen: Account
  },
  SettingAccount: {
    screen: SettingAccount
  },
  MyShop: {
    screen: MyShop
  },
  AktivasiMyShop: {
    screen: AktivasiMyShop
  },
  AddProduct:{
    screen: AddProduct
  },
  UpdateProduct:{
    screen: UpdateProduct
  },
  MyProduct:{
    screen: MyProduct
  },
  DetailProdukShop:{
    screen: DetailProdukShop
  },
  DetailShop:{
    screen: DetailShop
  },
  FormOrder:{
    screen: FormOrder
  },
  ToPay:{
    screen: ToPay
  },
  ToShip:{
    screen: ToShip
  },
  Receive:{
    screen: Receive
  },
  Completed:{
    screen: Completed
  },
  Cancelled:{
    screen: Cancelled
  },
  Return:{
    screen: Return
  },
  SUnpaid:{
    screen: SUnpaid
  },
  SToShip:{
    screen: SToShip
  },
  SShipping:{
    screen: SShipping
  },
  SCompleted:{
    screen: SCompleted
  },
  SCancelled:{
    screen: SCancelled
  },
  SReturn:{
    screen: SReturn
  },
  Investor:{
    screen: Investor
  },

  ChatHeader:{
    screen: ChatHeader
  },
  Chat:{
    screen: Chat
  }

});

const App = createAppContainer(NavStack);

export default App;

import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
  Form,
  Item,
  Label,
  Input,
  Textarea
} from "native-base";
import styles from "./styles/Login";
import colors from "../../styles/colors";
import GlobalConfig from "../components/GlobalConfig";

export default class LoginTentor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading : false,
      // email:'muhamatzaenalmahmut@gmail.com',
      // password:'zencode',
      email:'',
      password:'',
    };
  }

  static navigationOptions = {
    header: null
  };

  konfirmasiLogin(){
      if(this.state.email==""){
        Alert.alert(
          'Information',
          'Input email',
          [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          ],
          { cancelable: false }
        );
      }
      else if(this.state.password==""){
        Alert.alert(
          'Information',
          'Input Password',
          [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          ],
          { cancelable: false }
        );
      } else {
        var url = GlobalConfig.SERVERHOST + 'loginStudent';
        var formData = new FormData();
        formData.append("email", this.state.email)
        formData.append("password", this.state.password)

        fetch(url, {
          headers: {
            'Content-Type': 'multipart/form-data'
          },
          method: 'POST',
          body: formData
        }).then((response) => response.json())
          .then((response) => {
              if(response.status == 200) {
                AsyncStorage.setItem('profilStudent', JSON.stringify(response.data)).then(() => {
                  this.props.navigation.navigate("HomeMenuTentor");
                })
              } else if (response.status == 404) {
                  Alert.alert(
                    'Login',
                    'Email Not Found',
                    [
                      { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    ],
                    { cancelable: false }
                  );
              } else {
                  Alert.alert(
                    'Login',
                    'Password Salah',
                    [
                      { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    ],
                    { cancelable: false }
                  );
              }
          })
          .catch((error) => {
            Alert.alert('Cannot Log in', 'Check Your Internet Connection', [{
              text: 'Ok'
            }])
            console.log(error)
          })
      }
    }

  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={styles.homeWrapper}>
        <View
          style={{ flex: 1, flexDirection: "column", backgroundColor: "#fff" }}
        >
          <View>
            <ScrollView>
              <ImageBackground
                style={{
                  alignSelf: "center",
                  width: Dimensions.get("window").width,
                  height: 245,
                }}
                source={require("../../assets/images/header_tentor.png")}>
                <TouchableOpacity
                  transparent
                  style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:320}}
                  onPress={()=>this.props.navigation.navigate("MainMenu")}
                >
                  <Icon
                    name='arrow-back'
                    size={10}
                    style={{color:colors.white, fontSize:20}}
                  />
                </TouchableOpacity>
              </ImageBackground>
              <View style={{paddingLeft:25, paddingRight:25, marginTop:15}}>
                <Text style={{fontSize:50, color:'#315E64', fontWeight:'bold'}}>Hello</Text>
                <Text style={{fontSize:15, color:colors.black}}>Sign In to your account</Text>
              </View>
              <Card style={{ marginLeft: 25, marginRight: 25, borderRadius: 50, marginTop:40, paddingLeft:10, paddingRight:10}}>
                <Textarea style={{fontSize:14, color:colors.gray}} rowSpan={1.8} value={this.state.email} placeholder='Username' onChangeText={(text) => this.setState({ email: text })} />
              </Card>
              <Card style={{ marginLeft: 25, marginRight: 25, borderRadius: 50, marginTop:10, paddingLeft:10, paddingRight:10}}>
                <Textarea style={{fontSize:14, color:colors.gray}} rowSpan={1.8} secureTextEntry={true} value={this.state.password} placeholder='Password' onChangeText={(text) => this.setState({ password: text })} />
              </Card>
              <View style={{paddingLeft:25, paddingRight:25, marginTop:15}}>
                <Text style={{fontSize:12, color:colors.gray, textAlign:'right'}}>Forgot your Password?</Text>
              </View>
              <View style={{marginLeft:100, marginRight:100, marginTop:70}}>
                <Button
                  block
                  style={{
                    width:'100%',
                    height: 40,
                    marginBottom: 20,
                    borderWidth: 0,
                    backgroundColor: colors.primary,
                    borderRadius: 20
                  }}
                  onPress={() => this.konfirmasiLogin()}
                >
                  <Text style={{color:colors.white}}>Sign In</Text>
                </Button>
                <View style={{flex:1, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                  <Text style={{color:colors.gray, fontSize:12, textAlign:'center'}}>Don't have an account? </Text>
                  <TouchableOpacity
                    transparent
                    onPress={()=>this.props.navigation.navigate("RegisterTentor")}
                  >
                  <Text style={{color:colors.black, fontSize:12, fontWeight:'bold'}}>Create</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
        </View>
        </Container>
      );
  }
}

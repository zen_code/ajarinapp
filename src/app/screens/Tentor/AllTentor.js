import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
  Textarea
} from "native-base";
import styles from "./../styles/Tentor";
import colors from "../../../styles/colors";
import CustomFooterStudent from "../../components/CustomFooterStudent";
import Slideshow from "react-native-slideshow";
import PropTypes from 'prop-types';
import SubTentor from "../../components/SubTentor";
import Ripple from "react-native-material-ripple";
import loadProduk from "../../data/allProduk.json";
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";
import GlobalConfig3 from "../../components/GlobalConfig3";
import Moment from 'moment'

var that;
class ListTentor extends React.PureComponent {
  navigateToScreen(route, dataTentor) {
    AsyncStorage.setItem("dataTentor", JSON.stringify(dataTentor)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={{width:'100%'}}>
      <TouchableOpacity
        transparent
        onPress={() => this.navigateToScreen("DetailTentor", this.props.data)}>
          <SubTentor
            imageUri={{ uri: GlobalConfig2.SERVERHOST + "images/" + this.props.data.photo }}
            name={this.props.data.name}
            kampus={this.props.data.kampus}
            address={this.props.data.address}
            mapel={this.props.data.mapel}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

export default class DetailTentor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: false,
      listTentor:[],
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    AsyncStorage.getItem("profilStudent").then(profilStudent => {
      //alert(JSON.stringify(profilStudent))
    });
    this.loadTentor()
    // this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
    //   this.loadProduk()
    //   this.loadShop()
    // });
  }

  loadTentor(){
    var url = GlobalConfig.SERVERHOST + 'getAllDetailTentor';
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',

    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
              this.setState({
                  listTentor: responseJson.data,
                  isLoading: false
              });
          }
      })
  }

  navigateToScreen(route, filter) {
    AsyncStorage.setItem("filter", (JSON.stringify(filter))).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  _renderTentor = ({ item }) => <ListTentor data={item} />;

  render() {
    that=this;
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <ScrollView>
        <ImageBackground
          style={{
            alignSelf: "center",
            width: Dimensions.get("window").width,
            height: 200,
          }}
          source={require("../../../assets/images/header_list_tentor.png")}>
          <TouchableOpacity
            transparent
            style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:320}}
            onPress={()=>this.logOut()}
          >
          </TouchableOpacity>
        </ImageBackground>
        <Card style={{ marginLeft: 25, marginRight: 25, borderRadius: 50, marginTop:-25, paddingLeft:10, paddingRight:10, flex:1, flexDirection:'row'}}>
          <View style={{width:'5%'}}>
            <Icon
              name='search'
              style={{color:colors.gray, fontSize:22, paddingTop:13}}
            />
          </View>
          <View style={{width:'88%', paddingTop:4}}>
            <Textarea style={{fontSize:14, color:colors.gray}} rowSpan={1.8} value={this.state.username} placeholder='Search Tentor' onChangeText={(text) => this.setState({ username: text })} />
          </View>
          <View style={{width:'7%'}}>
            <Icon
              name='qr-scanner'
              style={{color:colors.gray, fontSize:22, paddingTop:13}}
            />
          </View>
        </Card>
          <View style={{width:'100%', marginTop:0, paddingBottom:5, backgroundColor:'#EEF0EF', paddingLeft:10, paddingRight:15}}>
            <View style={{marginTop:0}}>
            <ScrollView>
              <FlatList
                data={this.state.listTentor}
                renderItem={this._renderTentor}
                keyExtractor={(item, index) => index.toString()}
              />
            </ScrollView>
            </View>
          </View>
      </ScrollView>
      <CustomFooterStudent navigation={this.props.navigation} menu="Home" />

      </Container>
      );
  }
}

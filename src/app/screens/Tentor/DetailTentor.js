import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import styles from "./../styles/DetailProduk";
import colors from "../../../styles/colors";
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";

export default class DetailTentor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: false,
      dataTentor:[],
      customers_id:'',
      id:'',
      toko_id:'',
      name:'',
      deskripsi:'',
      harga:'',
      image_primary:'',
      jumlahProdukShop:'',
      visibleShare:false,
      like:false,
      konfimasiLike:''
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
      AsyncStorage.getItem('dataTentor').then((dataTentor) => {
        this.setState({
          dataTentor:dataTentor,
          id_tentor: JSON.parse(dataTentor).id_tentor,
          name: JSON.parse(dataTentor).name,
          kampus: JSON.parse(dataTentor).kampus,
          address: JSON.parse(dataTentor).address,
          mapel: JSON.parse(dataTentor).mapel,
          photo: JSON.parse(dataTentor).photo,
        });
      })
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {

      });
  }

  order(){
    Alert.alert(
      'Information',
      'Under Construction',
      [
        { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
      ],
      { cancelable: false }
    );
  }

  chat(){
    Alert.alert(
      'Information',
      'Under Construction',
      [
        { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
      ],
      { cancelable: false }
    );
  }

  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <ScrollView>
        <Content style={{ marginTop:0}}>
        <View>
          <View style={{width: '100%', height:300}}>
            <ImageBackground
              style={{
                alignSelf: "center",
                width: Dimensions.get("window").width,
                height: 170,
              }}
              source={require("../../../assets/images/header_detail_tentor.png")}>
                <TouchableOpacity
                  transparent
                  style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:320}}
                  onPress={()=>this.props.navigation.navigate("AllTentor")}
                >
                  <Icon
                    name='arrow-back'
                    size={10}
                    style={{color:colors.white, fontSize:20}}
                  />
                </TouchableOpacity>
            </ImageBackground>
            <View style={{flexDirection:'row', marginBottom:10, paddingTop:10, paddingBottom:10, marginTop:-70, borderRadius:10, backgroundColor:colors.white, marginLeft:10, marginRight:10}}>
              <View style={{width:'25%', justifyContent: "center", paddingLeft:5}}>
                <View style={{width:70, height:70, borderRadius:40}}>
                  <Image
                    source={{ uri: GlobalConfig2.SERVERHOST + "images/" + this.state.photo }}
                    style={{ width: '100%', height: '100%', marginTop: 0, marginBottom: 2, borderRadius:40,}}
                  />
                </View>
              </View>
              <View style={{width:'55%'}}>
                <Text style={{fontSize:12, color:colors.black, fontWeight:'bold'}}>{this.state.name}</Text>
                <Text style={{fontSize:10, color:colors.gray}}>{this.state.kampus}</Text>
                <View style={{flex:1, flexDirection:'row'}}>
                    <Icon
                        name='ios-pin'
                        style={{fontSize:15, color:colors.primary, paddingRight:5}}
                    />
                  <Text style={{fontSize:12, paddingTop:0}}>{this.state.address}</Text>
                </View>
                <View style={{flex:1, flexDirection:'row'}}>
                    <Icon
                        name='book'
                        style={{fontSize:15, color:colors.primary, paddingRight:5}}
                    />
                  <Text style={{fontSize:12, paddingTop:0}}>{this.state.mapel}</Text>
                </View>
              </View>
              <View style={{width:'20%'}}>
              <View style={{flexDirection:'row', flex:1, paddingTop:4}}>
                <View style={{paddingRight:2}}>
                  <Icon
                      name='ios-star'
                      style={{fontSize:10, color:'#EB9201'}}
                  />
                </View>
                <View style={{paddingRight:2}}>
                  <Icon
                      name='ios-star'
                      style={{fontSize:10, color:'#EB9201'}}
                  />
                </View>
                <View style={{paddingRight:2}}>
                  <Icon
                      name='ios-star'
                      style={{fontSize:10, color:'#EB9201'}}
                  />
                </View>
                <View style={{paddingRight:2}}>
                  <Icon
                      name='ios-star'
                      style={{fontSize:10, color:'#EB9201'}}
                  />
                </View>
                <View style={{paddingRight:2}}>
                  <Icon
                      name='ios-star'
                      style={{fontSize:10, color:'#DEDFDF'}}
                  />
                </View>
              </View>
              <View style={{width:50, height:50}}>
                <Image
                  source={require("../../../assets/images/qr1.png")}
                  style={{ width: '100%', height: '100%', marginTop: 0}}
                />
              </View>
              </View>
            </View>
          </View>

          <View style={{marginTop:-100}}>
            <View style={{width:'50%', paddingLeft:10}}>
              <Text style={{fontSize:12, color:colors.black}}>Kelas</Text>
            </View>
          </View>

          <View style={{flex:1, flexDirection:'row', marginTop:10, marginLeft:10}}>
            <View style={{width:100, height:100, backgroundColor:colors.white, marginRight:10, borderRadius:10, alignItems:'center', justifyContent:'center'}}>
              <Image
                source={require("../../../assets/images/kelas/biologi.png")}
                style={{ width: 50, height: 50, marginTop: 0}}
              />
            </View>

            <View style={{width:100, height:100, backgroundColor:colors.white, marginRight:10, borderRadius:10, alignItems:'center', justifyContent:'center'}}>
              <Image
                source={require("../../../assets/images/kelas/kimia.png")}
                style={{ width: 50, height: 50, marginTop: 0}}
              />
            </View>

            <View style={{width:100, height:100, backgroundColor:colors.white, marginRight:10, borderRadius:10, alignItems:'center', justifyContent:'center'}}>
              <Image
                source={require("../../../assets/images/kelas/matematika.png")}
                style={{ width: 50, height: 50, marginTop: 0}}
              />
            </View>

          </View>

          </View>
        </Content>
      </ScrollView>
      <Footer>
        <FooterTab style={{ backgroundColor: '#EEF0EF'}}>
          <View style={{flex:1, flexDirection:'row', paddingRight:10}}>
            <View style={{width:'20%', alignItems: 'center', justifyContent:'center'}}>
              <View style={{paddingTop:15, paddingBottom:15}}>
                <Button
                  transparent
                  onPress={() => this.chat()}
                >
                  <Image
                    source={require("../../../assets/images/Chat.png")}
                    style={{ width: 40, height: 40, resizeMode: "contain" }}
                  />
                </Button>
              </View>
            </View>
            <View style={{width:'80%', justifyContent: 'center'}}>
                <Button
                  block
                  style={{
                    width:'100%',
                    height: 40,
                    margin: 5,
                    borderWidth: 0,
                    backgroundColor: colors.secondary,
                    borderRadius: 25
                  }}
                  onPress={() => this.order()}
                >
                  <Text style={styles.textOrder}>ORDER</Text>
              </Button>
            </View>
          </View>
        </FooterTab>
      </Footer>
      <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleShare}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog }}
            onTouchOutside={() => {
              this.setState({ visibleShare: false });
            }}
            dialogTitle={<DialogTitle title="Share Product" />}
            actions={[
              <DialogButton
                style={{
                  fontSize: 11,
                  backgroundColor: colors.white,
                  borderColor: colors.blue01
                }}
                text="SORT"
                onPress={() => this.loadData()}
                // onPress={() => this.setState({ visibleFilter: false })}
              />
            ]}
          >
            <DialogContent>
              {
                <View style={{flexDirection:'row', flex:1, paddingTop:5}}>
                  <View style={{paddingRight:10}}>
                    <Icon
                        name='logo-facebook'
                        style={{fontSize:35, color:'#3850A0'}}
                    />
                  </View>
                  <View style={{paddingRight:10}}>
                    <Icon
                        name='logo-instagram'
                        style={{fontSize:35, color:'#E55098'}}
                    />
                  </View>
                  <View style={{paddingRight:10}}>
                    <Icon
                        name='logo-whatsapp'
                        style={{fontSize:35, color:'#29B573'}}
                    />
                  </View>
                  <View style={{paddingRight:0}}>
                    <Icon
                        name='logo-twitter'
                        style={{fontSize:35, color:'#2585C6'}}
                    />
                  </View>
                </View>
              }
            </DialogContent>
          </Dialog>
        </View>
      </Container>

      );
  }
}

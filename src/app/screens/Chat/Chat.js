import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Input,
  Thumbnail
} from "native-base";
import styles from "./../styles/Troli";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Slideshow from "react-native-slideshow";
import PropTypes from 'prop-types';
import SubProduk from "../../components/SubProduk";
import Ripple from "react-native-material-ripple";
import loadProduk from "../../data/allProduk.json";
import CheckBox from 'react-native-check-box';
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";
import Moment from 'moment'


var that;
class ListItem extends React.PureComponent {
  render() {
    return (
      <View style={{marginTop:7, marginLeft:0, marginRight:0, paddingLeft:10, paddingRight:5, width:'100%'}}>
          {this.props.data.customers_id==that.state.customers_id ? (
            <View style={{flex:1, flexDirection:'row'}}>
              <View style={{width:'60%'}}>
              </View>
              <View style={{width:'40%'}}>
                <View style={{backgroundColor:'#4385F5', paddingRight:10, paddingLeft:10, borderRadius:6, paddingTop:5, paddingBottom:5}}>
                  <Text style={{textAlign: 'right', color:colors.white}}>{this.props.data.message}</Text>
                </View>
                <View style={{paddingRight:10}}>
                  <Text style={{textAlign: 'right', fontSize:9}}>{this.props.data.date}</Text>
                </View>
              </View>
            </View>
          ):(
            <View style={{flex:1, flexDirection:'row'}}>
              <View style={{width:'40%'}}>
                <View style={{backgroundColor:'#F2F4F5', paddingRight:10, paddingLeft:10, borderRadius:6, paddingTop:5, paddingBottom:5}}>
                  <Text style={{}}>{this.props.data.message}</Text>
                </View>
                <View style={{paddingRight:10}}>
                  <Text style={{fontSize:9}}>{this.props.data.date}</Text>
                </View>
              </View>
              <View style={{width:'60%'}}>
              </View>
            </View>
          )
          }
      </View>
    );
  }
}


export default class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: false,
      customers_id:'',
      customers_name:'',
      idChat:'',
      listChat:[],
      message:''
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    AsyncStorage.getItem("profil").then(dataUser => {
        this.setState({
          customers_id: JSON.parse(dataUser).id,
        });
    });
    AsyncStorage.getItem("idChat").then(idChat => {
      this.setState({
        idChat:idChat
      })
      this.loadChat()
    });

    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      this.loadChat()
    });
  }


  loadChat(){
    var url = GlobalConfig.SERVERHOST + 'getChat';

    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',
    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
              this.setState({
                listChat: responseJson.data.filter(x => x.id_header == this.state.idChat),
              });
          }

      })
  }

  send(){
    if(this.state.message==''){
      Alert.alert('Information', 'Type your message', [{
        text: 'OK'
      }]);
    } else {
      this.sendConfirmation()
    }
  }
  sendConfirmation() {
    var date = Moment().format('hh:mm');
    var url = GlobalConfig.SERVERHOST + 'addChat';
    var formData = new FormData();
    formData.append("id_header", this.state.idChat)
    formData.append("customers_id", this.state.customers_id)
    formData.append("message", this.state.message)
    formData.append("date", date)
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'POST',
      body: formData
    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
            this.setState({
              message:''
            })
            this.loadChat()
          } else{
            Alert.alert('Error', 'Add Chat Failed', [{
              text: 'OK'
            }]);
          }
      })
  }

  onRefresh() {
    console.log("refreshing");
    this.setState(function() {
      this.loadChat();
    });
  }

  _renderItem = ({ item }) => <ListItem data={item} />;

  render() {
    that=this;
    var listChat;
    if (this.state.isLoading) {
      listChat = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
        if (this.state.listChat == '') {
          listChat = (
            <View
              style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
            >
              <Thumbnail
                square
                large
                source={require("../../../assets/images/messageEmpty.png")}
              />
              <Text>No Message!</Text>
            </View>
          );
        } else {
          listChat = (
            <FlatList
              data={this.state.listChat}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isLoading}
                  onRefresh={this.onRefresh.bind(this)}
                />
              }
            />
          );
        }
    }
    return (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
        <Left style={{ flex: 1 }}>
        <Button
          transparent
          onPress={() => this.props.navigation.navigate("ChatHeader")}
        >
          <Icon
            name='arrow-back'
            style={{fontSize:20, color:colors.black}}
          />
        </Button>
        </Left>
        <Body style={{flex:3, alignItems:'center' }}>
          <Title style={{fontSize:18, color:colors.black}}>Chat</Title>
        </Body>
        <Right style={{ flex: 1}}>
        </Right>
      </Header>
      <View style={{ marginTop: 5, flex: 1, paddingRight:5, flexDirection: "column" }}>
        {listChat}
      </View>
      <Footer>
        <FooterTab style={{ backgroundColor: '#EEF0EF'}}>
        <View style={{flex:1, flexDirection:'row'}}>
        <View style={styles.input}>
          <TextInput
            style={{ flex: 1, color:colors.black, paddingLeft:5, paddingRight:5}}
            value={this.state.message}
            onChangeText={message => this.setState({ message })}
            blurOnSubmit={false}
            onSubmitEditing={() => this.send()}
            placeholder="Type a message"
            returnKeyType="send"
          />
        </View>
        <View style={styles.inputBtn}>
          <TouchableOpacity
            transparent
            onPress={() => this.send()}
          >
          <Icon
            name='send'
            style={{fontSize:30, paddingLeft:5, paddingRight:4, color:colors.black}}
          />
          </TouchableOpacity>
        </View>
        </View>
        </FooterTab>
      </Footer>
      </Container>
      );
  }
}

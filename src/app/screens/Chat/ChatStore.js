import React from 'react';
import { View } from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import {Fab, Icon} from 'native-base'
import Header from './Header';
//import Calls from './CallsTab';
import Chats from './ChatsTab';
import Contacts from './ContactsTab';
import colors from "../../../styles/colors";
import loadChat from "../../data/chat.json";
import CustomFooter from "../../components/CustomFooter";
class ChatStore extends React.Component {

  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      Contacts: [],
      Chats: [],
      Calls: [],
    };

  }
  componentDidMount() {
    const loadData = loadChat;
    this.setState({
         Contacts: loadData.Contacts,
         Chats: loadData.Chats,
         Calls: loadData.Calls,
    })
}

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header />
        <ScrollableTabView
          style={{ borderColor: '#fff' }}
          tabBarUnderlineStyle={style = { backgroundColor: '#fff' }}
          tabBarBackgroundColor="#A7723E"
          tabBarActiveTextColor="#fff"
          tabBarInactiveTextColor="#fff"
          initialPage={0}
        >
          {/*<Calls
            tabLabel="Chat"
            CallsData={this.state.Calls}
            {...this.props}
          />*/}
          <Chats
            tabLabel="Chat"
            ChatsData={this.state.Chats}
            {...this.props}
          />
          <Contacts
            tabLabel="Store"
            ContactsData={this.state.Contacts}
            {...this.props}
          />
        </ScrollableTabView>
        <Fab
          active={this.state.active}
          direction="up"
          containerStyle={{}}
          style={{ backgroundColor: colors.secondary, marginBottom: 50 }}
          position="bottomRight"
          onPress={() =>
            this.props.navigation.navigate("DailyReportAktifitasCreate")
          }>
          <Icon
            name="ios-add"
            style={{
              fontSize: 50,
              fontWeight: "bold",
              paddingTop: 0
            }}
          />
        </Fab>
        <CustomFooter navigation={this.props.navigation} menu="Chatting" />
      </View>
    );
  }
}

export default ChatStore;

import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Input,
  Thumbnail
} from "native-base";
import styles from "./../styles/Troli";
import colors from "../../../styles/colors";
import CustomFooterStudent from "../../components/CustomFooterStudent";
import Slideshow from "react-native-slideshow";
import PropTypes from 'prop-types';
import SubProduk from "../../components/SubProduk";
import Ripple from "react-native-material-ripple";
import loadProduk from "../../data/allProduk.json";
import CheckBox from 'react-native-check-box';
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";

var that;
class ListItem extends React.PureComponent {
  navigateToScreen(route, idChat) {
    AsyncStorage.setItem("idChat", JSON.stringify(idChat)).then(() => {
      that.props.navigation.navigate(route);
    });
  }
  render() {
    return (
      <View style={{marginTop:2, marginLeft:0, marginRight:0, paddingLeft:10, paddingRight:5, backgroundColor:colors.white}}>
        <Ripple
          style={{
              flex: 2,
              justifyContent: "center",
              alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
              this.navigateToScreen("Chat", this.props.data.id)
          }
          rippleColor={colors.accent}
        >

        <View style={{flexDirection:'row', marginBottom:5, marginTop:5}}>
          <View style={{width:'18%', justifyContent: "center"}}>
            <View style={{width:50, height:50, borderRadius:50}}>
              <Image
                source={require("../../../assets/images/contact.png")}
                style={{ width: '100%', height: '100%',  borderRadius:50, marginTop: 0, marginBottom: 2 }}
              />
            </View>
          </View>
          <View style={{width:'82%'}}>
          {this.props.data.customers_name_receiver==that.state.customers_name ?(
            <Text style={{fontSize:12, color:colors.black, fontWeight:'bold'}}>{this.props.data.customers_name_sender}</Text>
          ):(
            <Text style={{fontSize:12, color:colors.black, fontWeight:'bold'}}>{this.props.data.customers_name_receiver}</Text>
          )}
          </View>
        </View>
        </Ripple>
      </View>
    );
  }
}

export default class ChatHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: true,
      customers_id:'',
      customers_name:'',
      listsHeaderChat:[],
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    AsyncStorage.getItem("profil").then(dataUser => {
        this.setState({
          customers_id: JSON.parse(dataUser).id,
          customers_name: JSON.parse(dataUser).name,
        });
        this.loadChatHeader();
    });

    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      this.loadChatHeader();
    });
  }

  onRefresh() {
    console.log("refreshing");
    this.setState({ isLoading: true }, function() {
      this.loadChatHeader();
    });
  }

  loadChatHeader(){
    this.setState({ isLoading: true });
    var url = GlobalConfig.SERVERHOST + 'getHeadChat';

    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',
    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
              this.setState({
                listHeaderChat: responseJson.data,
                isLoading: false
              });
          }

      })
  }

  _renderItem = ({ item }) => <ListItem data={item} />;

  render() {
    that=this;
    var listHeaderChat;
    if (this.state.isLoading) {
      listHeaderChat = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
        if (this.state.listHeaderChat == '') {
          listHeaderChat = (
            <View
              style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
            >
              <Thumbnail
                square
                large
                source={require("../../../assets/images/empty.png")}
              />
              <Text>No Data!</Text>
            </View>
          );
        } else {
          listHeaderChat = (
            <FlatList
              data={this.state.listHeaderChat}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isLoading}
                  onRefresh={this.onRefresh.bind(this)}
                />
              }
            />
          );
        }
    }
    return (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
        <Left style={{ flex: 1 }}>
        </Left>
        <Body style={{flex:3, alignItems:'center' }}>
          <Title style={{fontSize:18, color:colors.black}}>Chat</Title>
        </Body>
        <Right style={{ flex: 1}}>
        </Right>
      </Header>
      <View style={{ marginTop: 5, flex: 1, paddingRight:5, flexDirection: "column" }}>
        {listHeaderChat}
      </View>
      <CustomFooterStudent navigation={this.props.navigation} menu="Chatting" />
      </Container>
    );
  }
}

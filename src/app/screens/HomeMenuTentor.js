import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
  Textarea
} from "native-base";
import styles from "./styles/HomeMenu";
import colors from "../../styles/colors";
import CustomFooter from "../components/CustomFooter";
import Slideshow from "react-native-slideshow";
import PropTypes from 'prop-types';
import SubProduk from "../components/SubProduk";
import SubArtikel from "../components/SubArtikel";
import SubNewTentor from "../components/SubNewTentor";
import Ripple from "react-native-material-ripple";
import loadProduk from "../data/allProduk.json";
import GlobalConfig from "../components/GlobalConfig";
import GlobalConfig2 from "../components/GlobalConfig2";
import GlobalConfig3 from "../components/GlobalConfig3";
import Moment from 'moment'

export default class HomeMenuTentor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      first_name:'',
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    AsyncStorage.getItem("profilStudent").then(profilStudent => {
      //alert(JSON.stringify(profilStudent))
      this.setState({ first_name: JSON.parse(profilStudent).first_name });
        //this.loadData(JSON.parse(dataUser));
    });
  }
  isi(){
    Alert.alert(
      'Information',
      'Under Construction',
      [
        { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
      ],
      { cancelable: false }
    );
  }

    render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <ScrollView>
        <ImageBackground
          style={{
            alignSelf: "center",
            width: Dimensions.get("window").width,
            height: 200,
          }}
          source={require("../../assets/images/header_home.png")}>
          <TouchableOpacity
            transparent
            style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:320}}
            onPress={()=>this.logOut()}
          >
          </TouchableOpacity>
        </ImageBackground>
        <Card style={{ marginLeft: 25, marginRight: 25, borderRadius: 50, marginTop:-20, paddingLeft:10, paddingRight:10, flex:1, flexDirection:'row'}}>
          <View style={{width:'5%'}}>
            <Icon
              name='search'
              style={{color:colors.gray, fontSize:22, paddingTop:5}}
            />
          </View>
          <View style={{width:'88%', paddingTop:2, paddingLeft:5}}>
            <Text style={{paddingTop:5}}>Hallo {this.state.first_name}</Text>
          </View>
          <View style={{width:'7%'}}>
            <Icon
              name='qr-scanner'
              style={{color:colors.gray, fontSize:22, paddingTop:5}}
            />
          </View>
        </Card>
        <View style={{marginTop:200, marginLeft:130, marginRight:130}}>
        <Button
          block
          style={{
            width:'100%',
            marginTop:10,
            height: 35,
            marginBottom: 5,
            borderWidth: 0,
            backgroundColor: colors.primary,
            borderRadius: 15
          }}
          onPress={() => this.isi()}
        >
          <Text style={{color:colors.white}}>Isi Formulir</Text>
        </Button>
        </View>
      </ScrollView>
      </Container>
      );
  }
}

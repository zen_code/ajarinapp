import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon
} from "native-base";
import styles from "./../styles/AllProduk";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Slideshow from "react-native-slideshow";
import PropTypes from 'prop-types';
import SubProduk from "../../components/SubProduk";
import Ripple from "react-native-material-ripple";
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";

var that;
class ListItem extends React.PureComponent {
  navigateToScreen(route, listProduk) {
    AsyncStorage.setItem("listProduk", JSON.stringify(listProduk)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={{width:'50%', paddingRight:5}}>
      <TouchableOpacity
        transparent
        onPress={() => this.navigateToScreen("DetailProduk", this.props.data)}>
        <SubProduk
          imageUri={{ uri: GlobalConfig2.SERVERHOST + "images/" + this.props.data.image_primary }}
          name={this.props.data.name}
          harga={this.props.data.harga}
          terjual={this.props.data.stok}
          favorite={this.props.data.berat}
        />
        </TouchableOpacity>
      </View>
    );
  }
  }

export default class AllProduk extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: false,
      search:"Search",
      listProduk:[],
      filter:'',
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {

    this.loadProduk()

    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      this.loadProduk();
    });
  }

  loadProduk(){
    var url = GlobalConfig.SERVERHOST + 'get_product_all';
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',

    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
              this.setState({
                  listProduk: responseJson.data,
                  isLoading: false
              });
          }
      })
  }



  _renderItem = ({ item }) => <ListItem data={item} />;

  render() {
    that=this;
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
        <Left style={{ flex: 3 }}>
          <View style={styles.searchHeader}>
              <TextInput style={{height:40, marginLeft:10, marginRight:10}} value={this.state.search} onChangeText={(text) => this.setState({ search: text })} />
          </View>
        </Left>
        <Right style={{ flex: 2, marginLeft:15 }}>
          <Button
            transparent
            onPress={() => this.props.navigation.navigate("ScanToko")}
          >
            <Image
              source={require("../../../assets/images/QR.png")}
              style={{ width: 35, height: 35, resizeMode: "contain" }}
            />
          </Button>
          <Button
            transparent
            onPress={() => this.props.navigation.navigate("Troli")}
          >
            <Image
              source={require("../../../assets/images/Keranjang.png")}
              style={{ width: 30, height: 30, resizeMode: "contain" }}
            />
          </Button>
        </Right>
      </Header>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <ScrollView>
        <Content style={{ marginTop:5, paddingRight:5}}>
          <FlatList
            data={this.state.listProduk}
            renderItem={this._renderItem}
            numColumns={2}
            keyExtractor={(item, index) => index.toString()}
          />
        </Content>
      </ScrollView>
      <CustomFooter navigation={this.props.navigation} menu="Home" />

      </Container>
      );
  }
}

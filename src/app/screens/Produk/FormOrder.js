import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
  Form,
  Item,
  Label,
  Input,
  Textarea,
  Picker
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import styles from "./../styles/AllProduk";
import colors from "../../../styles/colors";
import CustomRadioButton from "../../components/CustomRadioButton";
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";
import ImagePicker from "react-native-image-picker";
import Ripple from "react-native-material-ripple";

export default class AddProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading : false,
      listShop:[],
      customers_id:'',
      customers_name:'',
      toko_id:'',
      kode:'',
      name:'',
      image:'',
      harga:'',
      status:'',
      qty:1,
      total:'',
      alamat:'',
      total:''
    };
  }

  static navigationOptions = {
    header: null
  };


  componentDidMount() {
      AsyncStorage.getItem('detailOrder').then((detailOrder) => {
        this.setState({
          id: JSON.parse(detailOrder).id,
          customers_id: JSON.parse(detailOrder).customers_id,
          customers_name: JSON.parse(detailOrder).customers_name,
          toko_id: JSON.parse(detailOrder).toko_id,
          name: JSON.parse(detailOrder).name,
          image: JSON.parse(detailOrder).image,
          harga: JSON.parse(detailOrder).harga,
        });
        this.loadShop()
        this.loadSum()
      })
  }



  loadShop(){
    var url = GlobalConfig.SERVERHOST + 'get_toko_all';
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',

    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
              this.setState({
                  listShop: responseJson.data.filter(x => x.id == this.state.toko_id),
                  isLoading: false
              });
          }
      })
  }

  konfirmasiCancel(){
    Alert.alert(
      'Confirmation',
      'Do you want to discard this product?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Discard', onPress: () => this.props.navigation.navigate("HomeMenu") },
      ],
      { cancelable: false }
    );
  }

  loadSum(){
    let harga=this.state.harga
    let jumlah=this.state.qty

    let total=harga * jumlah
    this.setState({
      total:total
    })
  }

  konfirmasiOrder(){
      if(this.state.alamat=='') {
        Alert.alert(
          'Information',
          'Input Address',
          [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          ],
          { cancelable: false }
        );
      } else{
        this.konfirmasiFinal()
      }

  }

  konfirmasiFinal()
  {
    Alert.alert(
      'Information',
      'Do you want to ORDER  this product?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Order', onPress: () => this.Order() },
      ],
      { cancelable: false }
    );
  }

  Order(){
    var kode = Math.floor((Math.random() * 10000000000000000) + 1);
    var url = GlobalConfig.SERVERHOST + 'updateOrder';
    var formData = new FormData();
    formData.append("id", this.state.id)
    formData.append("customers_id", this.state.customers_id)
    formData.append("customers_name", this.state.customers_name)
    formData.append("toko_id", this.state.toko_id)
    formData.append("kode", kode)
    formData.append("name", this.state.name)
    formData.append("image", this.state.image)
    formData.append("harga", this.state.harga)
    formData.append("status", 'order')
    formData.append("qty", this.state.qty)
    formData.append("total", this.state.total)
    formData.append("alamat", this.state.alamat)
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'POST',
      body: formData
    }).then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.status == 200) {
          Alert.alert(
            'Information',
            'CODE ORDER  : ' + responseJson.data.kode,
            [
              { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
            ],
            { cancelable: false }
          );
          this.props.navigation.navigate("ToPay");
        } else{
          Alert.alert('Error', 'Create Product Failed', [{
            text: 'OK'
          }]);
        }
    });
  }

  ubahQTY(jumlah) {
    let arr = this.state.qty;
    arr = jumlah;
    this.setState({
      qty: arr
    });
    this.loadSum()
  }

  tambahQTY() {
    let arr = this.state.qty;
    if (arr == 0 || arr == null || arr == undefined) {
    } else {
      arr++;
      this.setState({
        qty: arr
      });
    }
    this.loadSum()
  }

  kurangQTY() {
    let arr = this.state.qty;
    if (arr == 1 || arr == null || arr == undefined) {
    } else {
      arr--;
      this.setState({
        qty: arr
      });
    }
    this.loadSum()
  }

  render() {


    return this.state.isloading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
        <Left style={{ flex: 1 }}>
            <TouchableOpacity
              transparent
              onPress={()=>this.konfirmasiCancel()}
            >
              <Icon
                name='arrow-back'
                size={10}
                style={{color:colors.black, fontSize:20}}
              />
            </TouchableOpacity>
        </Left>
        <Body style={{ flex:3, alignItems:'center' }}>
          <Title style={{fontSize:15, color:colors.black}}>Order Product</Title>
        </Body>
        <Right style={{ flex: 1 }}>
          <TouchableOpacity
            transparent
            onPress={()=>this.konfirmasiOrder()}
          >
          <Icon
            name='checkmark'
            style={{paddingLeft:10, fontSize:20, color:colors.secondary}}
          />
          </TouchableOpacity>
        </Right>
      </Header>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={{ flex: 1 }}>
        <Content style={{ marginTop: 0 }}>
        <View></View>
          <View style={{ backgroundColor: '#FEFEFE' }}>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                <View style={{ flex: 1}}>
                    <View style={{paddingBottom:5}}>
                      <Text style={styles.titleInput}>Photo</Text>
                    </View>
                    <View>
                    <View style={{flex:1, flexDirection:'row'}}>
                      <View style={{marginRight:10}}>
                        <View style={styles.placeholder}>
                            <TouchableOpacity
                              transparent
                              style={{alignItems:'center', justifyContent:'center'}}
                              onPress={this.pickImageHandlerPrimary}
                            >
                              <Image source={{ uri: GlobalConfig2.SERVERHOST + "images/" + this.state.image }} style={styles.previewImage}/>
                            </TouchableOpacity>
                        </View>
                      </View>
                      <View>
                      </View>
                    </View>
                    </View>
                </View>
            </CardItem>

            {this.state.listShop.map((listShop, index) => (
            <View>
            <CardItem style={{ borderRadius: 0, marginTop:-10, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Name Shop</Text>
                  </View>
                  <View>
                    <Text style={{fontSize:12, marginLeft:5, color:colors.black}}>{listShop.name}</Text>
                  </View>
                </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:-10, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Alamat Shop</Text>
                  </View>
                  <View>
                    <Text style={{fontSize:12, marginLeft:5, color:colors.black}}>{listShop.alamat}</Text>
                  </View>
                </View>
            </CardItem>
            </View>
            ))}
            <CardItem style={{ borderRadius: 0, marginTop:-10, backgroundColor:colors.gray  }}>
              <View style={{ flex: 1 }}>
                <View>
                  <Text style={styles.titleInput}>Name Product </Text>
                </View>
                <View>
                  <Text style={{fontSize:12, marginLeft:5, color:colors.black}}>{this.state.name}</Text>
                </View>
              </View>
            </CardItem>

              <CardItem style={{ borderRadius: 0, marginTop:-10, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View style={{ flex: 1, flexDirection: "row"}}>
                    <View style={{width:'50%'}}>
                      <Text style={styles.titleInput}>Price </Text>
                    </View>
                    <View style={{width:'50%'}}>
                      <Text style={styles.titleInput}>Quantity </Text>
                    </View>
                  </View>
                  <View style={{ flex: 1, flexDirection: "row"}}>
                    <View style={{width:'50%',flex:1, flexDirection:'row'}}>
                        <View style={{width:'70%', paddingRight:5}}>
                          <Text style={{fontSize:15, marginLeft:5, color:colors.secondary, fontWeight:'bold'}}>Rp. {this.state.harga}</Text>
                        </View>
                    </View>
                      <View style={{ flex: 1, flexDirection: "row"}}>
                        <Button onPress={() => this.kurangQTY()} style={styles.btnQTYLeft}>
                            <Icon
                              name="ios-arrow-back"
                              style={styles.facebookButtonIconQTY}
                            />
                        </Button>

                        <View style={{width:50, height:30, marginTop: 10, backgroundColor: colors.white}}>
                            <Input
                              style={{
                                height:30,
                                marginTop:0,
                                fontSize:9,
                                textAlign:'center'}}
                              keyboardType='numeric'
                              value={this.state.qty + ""}
                              onChangeText={text => this.ubahQTY(text)}
                            />
                        </View>
                        <Button onPress={() => this.tambahQTY()} style={styles.btnQTYRight}>
                            <Icon
                              name="ios-arrow-forward"
                              style={styles.facebookButtonIconQTY}
                            />
                        </Button>
                      </View>
                    </View>
                  </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:-10, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Total</Text>
                  </View>
                  <View>
                    <Text style={{fontSize:15, marginLeft:5, color:colors.secondary, fontWeight:'bold'}}>Rp. {this.state.total}</Text>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:-10, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                  <Text style={styles.titleInput}>Address</Text>
                  </View>
                  <View>
                  <Textarea style={ {borderRadius:5, marginLeft:5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={3} bordered value={this.state.alamat} placeholder='Type Description ...' onChangeText={(text) => this.setState({ alamat: text })} />
                  </View>
                </View>
              </CardItem>
          </View>
        </Content>
      </View>

      </Container>
      );
  }
}

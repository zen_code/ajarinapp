import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import styles from "./../styles/DetailProduk";
import colors from "../../../styles/colors";
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";

export default class DetailProduk extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: false,
      listShop:[],
      customers_id:'',
      id:'',
      toko_id:'',
      name:'',
      deskripsi:'',
      harga:'',
      image_primary:'',
      jumlahProdukShop:'',
      visibleShare:false,
      like:false,
      konfimasiLike:''
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
      AsyncStorage.getItem("profil").then(dataUser => {
          this.setState({
            customers_id: JSON.parse(dataUser).id,
            customers_name: JSON.parse(dataUser).name,
          });
      });

      AsyncStorage.getItem('listProduk').then((listProduk) => {
        this.setState({
          listProduk:listProduk,
          id: JSON.parse(listProduk).id,
          toko_id: JSON.parse(listProduk).toko_id,
          name: JSON.parse(listProduk).name,
          deskripsi: JSON.parse(listProduk).deskripsi,
          harga: JSON.parse(listProduk).harga,
          image_primary: JSON.parse(listProduk).image_primary,
        });
        this.loadShop()
        this.loadAll()

      })
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        this.loadShop();
      });
  }

  loadAll(){
    var url = GlobalConfig.SERVERHOST + 'get_product_all';
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',

    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
              this.setState({
                  jumlahProdukShop: responseJson.data.filter(x => x.toko_id == this.state.toko_id).length,
              });
          }
      })
  }

  loadShop(){
    var url = GlobalConfig.SERVERHOST + 'get_toko_all';
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',

    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
              this.setState({
                  listShop: responseJson.data.filter(x => x.id == this.state.toko_id),
                  isLoading: false
              });
          }
      })
  }

  loadShare(){
    this.setState({
      visibleShare: true
    });
  }

  like(){
    this.setState({
      like:!this.state.like,
    });

    if(this.state.konfimasiLike == ''){
      this.setState({
        konfimasiLike:'yes'
      });
      this.saveFavorite()
    }
    if(this.state.like == false) {
      Alert.alert('Success', 'Save Favorite', [{
        text: 'OK'
      }]);
    }
  }

  saveFavorite(){
    var url = GlobalConfig.SERVERHOST + 'addFavorite';
    var formData = new FormData();
    formData.append("id_produk", this.state.id)
    formData.append("customers_id", this.state.customers_id)
    formData.append("toko_id", this.state.toko_id)
    formData.append("name", this.state.name)
    formData.append("deskripsi", this.state.deskripsi)
    formData.append("harga", this.state.harga)
    formData.append("image_primary", this.state.image_primary)

    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'POST',
      body: formData
    }).then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.status == 200) {
          this.componentDidMount()
        } else{
          Alert.alert('Error', 'Create Product Failed', [{
            text: 'OK'
          }]);
        }
    });
  }

  navigateToScreen(route, idToko) {
    AsyncStorage.setItem("idToko", idToko).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  navigateToScreenChat(route, idChat) {
    AsyncStorage.setItem("idChat", JSON.stringify(idChat)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  konfirmasiAddTrolli(){
    Alert.alert(
      'Confirmation',
      'Do you want to ADD this product?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Yes', onPress: () => this.AddTrolli() },
      ],
      { cancelable: false }
    );
  }

  AddTrolli(){
    var url = GlobalConfig.SERVERHOST + 'addOder';
    var formData = new FormData();
    formData.append("customers_id", this.state.customers_id)
    formData.append("customers_name", this.state.customers_name)
    formData.append("toko_id", this.state.toko_id)
    formData.append("name", this.state.name)
    formData.append("image", this.state.image_primary)
    formData.append("harga", this.state.harga)
    formData.append("status", 'troli')

    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'POST',
      body: formData
    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
            Alert.alert('Success', 'Add Trolli Success', [{
              text: 'OK'
            }]);
          } else{
            Alert.alert('Error', 'Add Trolli Failed', [{
              text: 'OK'
            }]);
          }
      })
  }

  order(){
    var url = GlobalConfig.SERVERHOST + 'addOder';
    var formData = new FormData();
    formData.append("customers_id", this.state.customers_id)
    formData.append("customers_name", this.state.customers_name)
    formData.append("toko_id", this.state.toko_id)
    formData.append("name", this.state.name)
    formData.append("image", this.state.image_primary)
    formData.append("harga", this.state.harga)
    formData.append("status", 'troli')

    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'POST',
      body: formData
    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
            this.props.navigation.navigate("Troli")
          } else{
            Alert.alert('Error', 'Add Trolli Failed', [{
              text: 'OK'
            }]);
          }
      })
  }

  addHeader(listShopName, listShopId){
    var url = GlobalConfig.SERVERHOST + 'addHeadChat';
    var formData = new FormData();
    formData.append("customers_id_sender", this.state.customers_id)
    formData.append("customers_name_sender", this.state.customers_name)
    formData.append("customers_id_receiver", listShopId)
    formData.append("customers_name_receiver", listShopName)

    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'POST',
      body: formData
    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
            this.navigateToScreenChat("Chat", JSON.parse(responseJson.data.id))
          } else{
            Alert.alert('Error', 'Add Trolli Failed', [{
              text: 'OK'
            }]);
          }
      })
  }

  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <ScrollView>
        <Content style={{ marginTop:0}}>
          <View style={{width: '100%', height:300}}>
            <Image
              style={{
                marginRight:10,
                alignSelf: "center",
                width: '100%',
                height: '100%',
                borderBottomRightRadius:50
              }}
              source={{ uri: GlobalConfig2.SERVERHOST + "images/" + this.state.image_primary }}/>
            <TouchableOpacity
              transparent
              style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:320}}
              onPress={()=>this.props.navigation.navigate("HomeMenu")}
            >
              <Icon
                name='arrow-back'
                size={10}
                style={{color:colors.white, fontSize:20}}
              />
            </TouchableOpacity>
          </View>
          <View style={{width: '100%', height:50, marginTop:-30, flex:1, flexDirection:'row'}}>
            <View style={{width:'60%'}}>
            </View>
            <View style={{width:'40%', height:50, justifyContent: "center", alignItems: "center"}}>
              <View style={{width:50, height:50, borderRadius:40, backgroundColor:colors.white, justifyContent: "center", alignItems: "center"}}>
                <TouchableOpacity
                  transparent
                  onPress={()=>this.like()}
                >
                  {this.state.like==false ?(
                  <Icon
                    name='ios-heart'
                    size={10}
                    style={{fontSize:30, color:colors.graydark}}
                  />
                  ):(
                  <Icon
                    name='ios-heart'
                    size={10}
                    style={{fontSize:30, color:colors.red}}
                  />
                  )}
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={{width:'100%', paddingLeft:10, paddingRight:10, marginTop:-5,}}>
            <Text style={{fontSize:15, color:colors.black, fontWeight:'bold'}}>{this.state.name}</Text>
            <Text style={{fontSize:15, paddingTop:5, color:colors.primarydark, fontWeight:'bold'}}>Rp.{this.state.harga}</Text>
            <Text style={{fontSize:12, paddingTop:0,}}>{this.state.deskripsi}</Text>
          </View>
          <View style={{flex:1, flexDirection:'row', paddingLeft:10, paddingRight:10, marginTop:10}}>
              <View style={{width:'40%', flex:1, flexDirection:'row', paddingTop:10, paddingLeft:10, paddingBottom:10, borderWidth:1, borderRadius:5, borderColor:colors.graydark, alignItems: 'center', justifyContent:'center'}}>
                <Icon
                  name='ios-cart'
                  style={{fontSize:15, paddingRight:10}}
                />
                <Text style={{fontSize:10, paddingRight:10}}>Sold</Text>
                <Text style={{fontSize:10}}>0</Text>
              </View>
              <View style={{width:'40%', flex:1, flexDirection:'row', paddingTop:10, paddingLeft:10, paddingBottom:10, borderWidth:1, borderRadius:5, borderColor:colors.graydark, alignItems: 'center', justifyContent:'center', marginRight:5, marginLeft:5}}>
                <Icon
                  name='ios-star-outline'
                  style={{fontSize:15, paddingRight:10}}
                />
                <Text style={{fontSize:10, paddingRight:5}}>Rating</Text>
                <Text style={{fontSize:10}}>0</Text>
              </View>
              <View style={{width:'20%', flex:1, flexDirection:'row', paddingTop:10, paddingLeft:10, paddingBottom:10, borderWidth:1, borderRadius:5, borderColor:colors.graydark, alignItems: 'center', justifyContent:'center'}}>
                <TouchableOpacity
                  transparent
                  onPress={()=>this.loadShare()}
                >
                  <Icon
                    name='share'
                    style={{fontSize:15, paddingRight:10}}
                  />
                </TouchableOpacity>
              </View>
          </View>
          {this.state.listShop.map((listShop, index) => (
          <View>
          <View style={{width:'100%', paddingLeft:10, paddingRight:10, marginTop:10}}>
            <View style={{borderRadius:10, backgroundColor:colors.gray, flex:1, flexDirection:'row'}}>
              <View style={{width:'50%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5}}>
                <View style={{width:'35%'}}>
                  <Image
                    style={{
                      width: 50,
                      height: 50,
                      borderRadius:50
                    }}
                    source={{ uri: GlobalConfig2.SERVERHOST + "images/" + listShop.image }}/>
                </View>
                <View style={{width:'65%'}}>
                  <Text style={{fontSize:12, paddingTop:0, fontWeight:'bold', color:colors.black}}>{listShop.name}</Text>
                  <Text style={{fontSize:10, paddingTop:0}}>Active 5 minutes ago</Text>
                  <View style={{flex:1, flexDirection:'row'}}>
                    <Icon
                        name='ios-pin'
                        style={{fontSize:15, color:colors.black, paddingRight:5}}
                    />
                    <Text style={{fontSize:12, paddingTop:0}}>{listShop.alamat}</Text>
                  </View>
                </View>
              </View>
              <View style={{width:'50%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5}}>
                <View style={{width:'50%', justifyContent: "center", alignItems: "center"}}>
                  <Text style={{fontSize:15, color:colors.black, fontWeight:'bold'}}>{this.state.jumlahProdukShop}</Text>
                  <Text style={{fontSize:10}}>Products</Text>
                </View>
                <View style={{width:'50%', justifyContent: "center", alignItems: "center"}}>
                  <Text style={{fontSize:15, color:colors.black, fontWeight:'bold'}}>0</Text>
                  <Text style={{fontSize:10}}>Rating</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={{width:'100%', paddingLeft:10, paddingRight:10, marginTop:10}}>
            <View style={{flex:1, flexDirection:'row'}}>
              <View style={{width:'50%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5, borderWidth:1, borderRadius:30, borderColor:colors.secondary, alignItems: 'center', justifyContent:'center', marginRight:5}}>
                <TouchableOpacity
                  transparent
                  onPress={() => this.navigateToScreen("DetailShop", this.state.toko_id)}
                >
                <Icon
                  name='ios-basket'
                  style={{fontSize:15, color:colors.secondary, paddingRight:10}}
                />
                </TouchableOpacity>
                <TouchableOpacity
                  transparent
                  onPress={() => this.navigateToScreen("DetailShop", this.state.toko_id)}
                >
                <Text style={{fontSize:12, fontWeight:'bold', color:colors.secondary}}>View Shop</Text>
                </TouchableOpacity>
              </View>
              <View style={{width:'50%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5, borderWidth:1, borderRadius:30, borderColor:colors.secondary, alignItems: 'center', justifyContent:'center', marginLeft:5}}>
                <TouchableOpacity
                  transparent
                  onPress={()=>this.props.navigation.navigate("HomeMenu")}
                >
                <Icon
                  name='ios-chatboxes'
                  style={{fontSize:15, color:colors.secondary, paddingRight:10}}
                />
                </TouchableOpacity>
                <TouchableOpacity
                  transparent
                  onPress={()=>this.addHeader(listShop.name, listShop.user_id)}
                >
                <Text style={{fontSize:12, fontWeight:'bold', color:colors.secondary}}>Chat Shop</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          </View>
        ))}
        </Content>
      </ScrollView>
      <Footer>
        <FooterTab style={{ backgroundColor: colors.gray}}>
          <View style={{flex:1, flexDirection:'row', paddingRight:10}}>
            <View style={{width:'20%', alignItems: 'center', justifyContent:'center'}}>
              <View style={{paddingTop:15, paddingBottom:15}}>
                <Button
                  transparent
                  onPress={() => this.konfirmasiAddTrolli()}
                >
                  <Image
                    source={require("../../../assets/images/Keranjang.png")}
                    style={{ width: 40, height: 40, resizeMode: "contain" }}
                  />
                </Button>
              </View>
            </View>
            <View style={{width:'80%', justifyContent: 'center'}}>
                <Button
                  block
                  style={{
                    width:'100%',
                    height: 40,
                    margin: 5,
                    borderWidth: 0,
                    backgroundColor: colors.secondary,
                    borderRadius: 25
                  }}
                  onPress={() => this.order()}
                >
                  <Text style={styles.textOrder}>BUY NOW</Text>
              </Button>
            </View>
          </View>
        </FooterTab>
      </Footer>
      <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleShare}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog }}
            onTouchOutside={() => {
              this.setState({ visibleShare: false });
            }}
            dialogTitle={<DialogTitle title="Share Product" />}
            actions={[
              <DialogButton
                style={{
                  fontSize: 11,
                  backgroundColor: colors.white,
                  borderColor: colors.blue01
                }}
                text="SORT"
                onPress={() => this.loadData()}
                // onPress={() => this.setState({ visibleFilter: false })}
              />
            ]}
          >
            <DialogContent>
              {
                <View style={{flexDirection:'row', flex:1, paddingTop:5}}>
                  <View style={{paddingRight:10}}>
                    <Icon
                        name='logo-facebook'
                        style={{fontSize:35, color:'#3850A0'}}
                    />
                  </View>
                  <View style={{paddingRight:10}}>
                    <Icon
                        name='logo-instagram'
                        style={{fontSize:35, color:'#E55098'}}
                    />
                  </View>
                  <View style={{paddingRight:10}}>
                    <Icon
                        name='logo-whatsapp'
                        style={{fontSize:35, color:'#29B573'}}
                    />
                  </View>
                  <View style={{paddingRight:0}}>
                    <Icon
                        name='logo-twitter'
                        style={{fontSize:35, color:'#2585C6'}}
                    />
                  </View>
                </View>
              }
            </DialogContent>
          </Dialog>
        </View>
      </Container>

      );
  }
}

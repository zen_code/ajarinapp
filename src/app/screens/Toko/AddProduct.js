import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
  Form,
  Item,
  Label,
  Input,
  Textarea,
  Picker
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import styles from "./../styles/AllProduk";
import colors from "../../../styles/colors";
import CustomRadioButton from "../../components/CustomRadioButton";
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";
import ImagePicker from "react-native-image-picker";
import Ripple from "react-native-material-ripple";

export default class AddProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading : false,
      idToko:'',
      listCategory:[],
      pickedImagePrimary: '',
      uriPrimary: '',
      fileTypePrimary:'',
      pickedImageSecondary: '',
      uriSecondary: '',
      fileTypeSecondary:'',
      stok:0,
      name:'',
      category_id:'',
      deskripsi:'',
      berat:'',
      harga:'',
      stok:'',
    };
  }

  static navigationOptions = {
    header: null
  };


  componentDidMount() {
      AsyncStorage.getItem('listShop').then((listShop) => {
        this.setState({
          idToko: JSON.parse(listShop).id,
        });
      })
      this.loadCategory()
  }

  loadCategory(){
    var url = GlobalConfig.SERVERHOST + 'get_category';
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',

    }).then((response) => response.json())
      .then((response) => {
          if(response.status == 200) {
              this.setState({ listCategory: response.data });
          }
      })
  }

  pickImageHandlerPrimary = () => {
    ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
        if (res.didCancel) {
            console.log("User cancelled!");
        } else if (res.error) {
            console.log("Error", res.error);
        } else {
            this.setState({
                pickedImagePrimary: res.uri,
                uriPrimary: res.uri,
                fileTypePrimary:res.type
            });
        }
    });
  }

  pickImageHandlerSecondary = () => {
    ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
        if (res.didCancel) {
            console.log("User cancelled!");
        } else if (res.error) {
            console.log("Error", res.error);
        } else {
            this.setState({
                pickedImageSecondary: res.uri,
                uriSecondary: res.uri,
                fileTypeSecondary:res.type
            });
        }
    });
  }

  konfirmasiCancel(){
    Alert.alert(
      'Confirmation',
      'Do you want to discard this product?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Discard', onPress: () => this.backMyShop() },
      ],
      { cancelable: false }
    );
  }

  backMyShop(){
    this.props.navigation.navigate('MyShop')
  }

  konfirmasiCreate(){
    if(this.state.uriPrimary==""){
      Alert.alert(
        'Information',
        'Upload Your Product',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    } else if(this.state.name==""){
      Alert.alert(
        'Information',
        'Input Name Product',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.category_id==""){
      Alert.alert(
        'Information',
        'Input Category',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.deskripsi==""){
      Alert.alert(
        'Information',
        'Input Description',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    } else if(this.state.berat==""){
      Alert.alert(
        'Information',
        'Input Weight',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    } else if(this.state.harga==""){
      Alert.alert(
        'Information',
        'Input Price',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.stok==""){
      Alert.alert(
        'Information',
        'Input Stock',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    } else {
          this.konfirmasiFinal();
      }
  }

  konfirmasiFinal(){
    Alert.alert(
      'Confirmation',
      'Do you want to SAVE this product?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Yes', onPress: () => this.saveProduct() },
      ],
      { cancelable: false }
    );
  }

  saveProduct(){
    var url = GlobalConfig.SERVERHOST + 'add_product';
    var formData = new FormData();
    formData.append("toko_id", this.state.idToko)
    formData.append("category_id", this.state.category_id)
    formData.append("name", this.state.name)
    formData.append("deskripsi", this.state.deskripsi)
    formData.append("berat", this.state.berat)
    formData.append("harga", this.state.harga)
    formData.append("stok", this.state.stok)
    if (this.state.uriPrimary!='') {
      formData.append("image_primary", {
        uri: this.state.uriPrimary,
        type: this.state.fileTypePrimary,
        name: 'imageproduk_' + this.state.name
    });
    }
    if (this.state.uriSecondary!='') {
      formData.append("image_secondary", {
        uri: this.state.uriSecondary,
        type: this.state.fileTypeSecondary,
        name: 'imageproduk2_' + this.state.name
    });
    }
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'POST',
      body: formData
    }).then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.status == 200) {
          Alert.alert('Success', 'Create Product Success', [{
            text: 'OK'
          }]);
          this.props.navigation.navigate("MyShop");
        } else{
          Alert.alert('Error', 'Create Product Failed', [{
            text: 'OK'
          }]);
        }
    });
  }

  ubahStock(jumlah) {
    let arr = this.state.stok;
    arr = jumlah;
    this.setState({
      stok: arr
    });
  }

  tambahStock() {
    let arr = this.state.stok;
    arr++;
    this.setState({
      stok: arr
    });
  }

  kurangStock() {
    let arr = this.state.stok;
    if (arr == 0 || arr == null || arr == undefined) {
    } else {
      arr--;
      this.setState({
        stok: arr
      });
    }
  }

  render() {
    let listCategory = this.state.listCategory.map( (s, i) => {
      return <Picker.Item key={i} value={s.id} label={s.name}/>
    });

    return this.state.isloading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
        <Left style={{ flex: 1 }}>
            <TouchableOpacity
              transparent
              onPress={()=>this.konfirmasiCancel()}
            >
              <Icon
                name='arrow-back'
                size={10}
                style={{color:colors.black, fontSize:20}}
              />
            </TouchableOpacity>
        </Left>
        <Body style={{ flex:3, alignItems:'center' }}>
          <Title style={{fontSize:15, color:colors.black}}>Add Product</Title>
        </Body>
        <Right style={{ flex: 1 }}>
          <TouchableOpacity
            transparent
            onPress={()=>this.konfirmasiCreate()}
          >
          <Icon
            name='checkmark'
            style={{paddingLeft:10, fontSize:20, color:colors.secondary}}
          />
          </TouchableOpacity>
        </Right>
      </Header>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={{ flex: 1 }}>
        <Content style={{ marginTop: 0 }}>
        <View></View>
          <View style={{ backgroundColor: '#FEFEFE' }}>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                <View style={{ flex: 1}}>
                    <View style={{paddingBottom:5}}>
                      <Text style={styles.titleInput}>Photo</Text>
                    </View>
                    <View>
                    <View style={{flex:1, flexDirection:'row'}}>
                      <View style={{marginRight:10}}>
                        <View style={styles.placeholder}>
                            <TouchableOpacity
                              transparent
                              style={{alignItems:'center', justifyContent:'center'}}
                              onPress={this.pickImageHandlerPrimary}
                            >
                              <Image source={{uri:this.state.pickedImagePrimary}} style={styles.previewImage}/>
                            </TouchableOpacity>
                        </View>
                      </View>
                      <View>
                      {this.state.uriPrimary!='' && (
                      <View style={styles.placeholder}>
                          <TouchableOpacity
                            transparent
                            style={{alignItems:'center', justifyContent:'center'}}
                            onPress={this.pickImageHandlerSecondary}
                          >
                            <Image source={{uri:this.state.pickedImageSecondary}} style={styles.previewImage}/>
                          </TouchableOpacity>

                      </View>
                      )}
                      </View>
                    </View>
                    </View>
                </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:-10, backgroundColor:colors.gray  }}>
              <View style={{ flex: 1 }}>
                <View>
                <Text style={styles.titleInput}>Name Product *</Text>
                </View>
                <View>
                <Textarea style={ {borderRadius:5, marginLeft:5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={2} bordered value={this.state.name} placeholder='Type Name Product ...' onChangeText={(text) => this.setState({ name: text })} />
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:-15, backgroundColor:colors.gray  }}>
              <View style={{ flex: 1}}>
                <View>
                  <Text style={styles.titleInput}>Category *</Text>
                  <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                      <Picker
                          mode="dropdown"
                          iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                          style={{ width: '100%', height:35}}
                          placeholder="Select Status ..."
                          placeholderStyle={{ color: "#bfc6ea" }}
                          placeholderIconColor="#007aff"
                          selectedValue={this.state.category_id}
                          onValueChange={(itemValue) => this.setState({category_id:itemValue})}>
                          <Picker.Item label="Choose Category"  value=""/>
                          {listCategory}
                      </Picker>
                  </Form>
                </View>
              </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:-10, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                  <Text style={styles.titleInput}>Description *</Text>
                  </View>
                  <View>
                  <Textarea style={ {borderRadius:5, marginLeft:5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={3} bordered value={this.state.deskripsi} placeholder='Type Description ...' onChangeText={(text) => this.setState({ deskripsi: text })} />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:-10, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View style={{ flex: 1, flexDirection: "row"}}>
                    <View style={{width:'50%'}}>
                      <Text style={styles.titleInput}>Weight *</Text>
                    </View>
                    <View style={{width:'50%'}}>
                      <Text style={styles.titleInput}>Stock *</Text>
                    </View>
                  </View>
                  <View style={{ flex: 1, flexDirection: "row"}}>
                    <View style={{width:'50%',flex:1, flexDirection:'row'}}>
                        <View style={{width:'70%', paddingRight:5}}>
                            <Textarea style={{borderRadius:5, marginLeft:5, fontSize:10}} keyboardType='numeric' rowSpan={1.5} bordered value={this.state.berat} placeholder='Type Weight ...' onChangeText={(text) => this.setState({ berat: text })} />
                        </View>
                        <View style={{alignItems:'center', justifyContent:'center'}}>
                            <Text>KG</Text>
                        </View>
                    </View>
                      <View style={{ flex: 1, flexDirection: "row"}}>
                        <Button onPress={() => this.kurangStock()} style={styles.btnQTYLeft}>
                            <Icon
                              name="ios-arrow-back"
                              style={styles.facebookButtonIconQTY}
                            />
                        </Button>

                        <View style={{width:50, height:30, marginTop: 10, backgroundColor: colors.white}}>
                            <Input
                              style={{
                                height:30,
                                marginTop:0,
                                fontSize:9,
                                textAlign:'center'}}
                              keyboardType='numeric'
                              value={this.state.stok + ""}
                              onChangeText={text => this.ubahStock(text)}
                            />
                        </View>
                        <Button onPress={() => this.tambahStock()} style={styles.btnQTYRight}>
                            <Icon
                              name="ios-arrow-forward"
                              style={styles.facebookButtonIconQTY}
                            />
                        </Button>
                      </View>
                    </View>
                  </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:-10, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View style={{ flex: 1, flexDirection: "row"}}>
                    <View style={{width:'50%'}}>
                      <Text style={styles.titleInput}>Price *</Text>
                    </View>
                  </View>
                  <View style={{ flex: 1, flexDirection: "row"}}>
                    <View style={{flex:1, flexDirection:'row'}}>
                        <View style={{width:'10%', alignItems:'center', justifyContent:'center'}}>
                            <Text>Rp</Text>
                        </View>
                        <View style={{width:'40%', paddingRight:5}}>
                            <Textarea style={{borderRadius:5, marginLeft:5, fontSize:10}} keyboardType='numeric' rowSpan={1.5} bordered value={this.state.harga} placeholder='Tye Price ...' onChangeText={(text) => this.setState({ harga: text })} />
                        </View>
                    </View>
                  </View>
                </View>
              </CardItem>
          </View>
        </Content>
      </View>

      </Container>
      );
  }
}

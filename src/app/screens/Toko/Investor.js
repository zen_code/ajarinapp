import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Input,
  Thumbnail
} from "native-base";
import styles from "./../styles/Troli";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Slideshow from "react-native-slideshow";
import PropTypes from 'prop-types';
import SubProduk from "../../components/SubProduk";
import Ripple from "react-native-material-ripple";
import loadProduk from "../../data/allProduk.json";
import CheckBox from 'react-native-check-box';
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";

class ListItem extends React.PureComponent {
  render() {
    return (
      <View style={{marginTop:2, marginLeft:0, marginRight:0, paddingLeft:10, paddingRight:5, backgroundColor:colors.white}}>
        <View style={{flexDirection:'row', marginBottom:5, marginTop:5}}>
          <View style={{width:'25%', justifyContent: "center"}}>
            <View style={{width:75, height:75}}>
              <Image
                source={{ uri: GlobalConfig2.SERVERHOST + "images/" + this.props.data.image }}
                style={{ width: '100%', height: '100%', marginTop: 0, marginBottom: 2 }}
              />
            </View>
          </View>
          <View style={{width:'75%'}}>
            <Text style={{fontSize:12, color:colors.black, fontWeight:'bold'}}>{this.props.data.nama}</Text>
            <Text style={{fontSize:10}}>{this.props.data.deskripsi}</Text>
            <Text style={{fontSize:10}}>Address : {this.props.data.alamat}</Text>
            <Text style={{fontSize:10}}>{this.props.data.tlp}</Text>
            <Text style={{fontSize:10}}>{this.props.data.email}</Text>
            <Text style={{fontSize:10, color:colors.secondary, fontWeight:'bold'}}>Investasi : Rp. {this.props.data.pinjaman}</Text>
          </View>
        </View>
      </View>
    );
  }
}

export default class Investor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: false,
      listInvestor:[],
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    this.loadInvestor()
  }

  loadInvestor(){
    var url = GlobalConfig.SERVERHOST + 'getInvestor';
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',
    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.code == 200) {
              this.setState({
                listInvestor: responseJson.data,
              });
          }

      })
  }

  _renderItem = ({ item }) => <ListItem data={item} />;

  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
        <Left style={{ flex: 1 }}>
        <Button
          transparent
          onPress={() => this.props.navigation.navigate("MyShop")}
        >
          <Icon
            name='arrow-back'
            style={{fontSize:20, color:colors.black}}
          />
        </Button>
        </Left>
        <Body style={{flex:3, alignItems:'center' }}>
          <Title style={{fontSize:18, color:colors.black}}>Investor</Title>
        </Body>
        <Right style={{ flex: 1}}>

        </Right>
      </Header>
      <ScrollView>
      <Content style={{marginRight:5}}>
        <FlatList
          data={this.state.listInvestor}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index.toString()}
        />
      </Content>
      </ScrollView>

      </Container>
      );
  }
}

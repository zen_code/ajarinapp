import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Input,
  Thumbnail
} from "native-base";
import styles from "./../styles/Troli";
import colors from "../../../styles/colors";
import CustomFooterStudent from "../../components/CustomFooterStudent";
import Slideshow from "react-native-slideshow";
import PropTypes from 'prop-types';
import SubProduk from "../../components/SubProduk";
import Ripple from "react-native-material-ripple";
import loadProduk from "../../data/allProduk.json";
import CheckBox from 'react-native-check-box';
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";

export default class Jadwal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      history:[],
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {

  }


  render() {
    var listHistory;
    if (this.state.isLoading) {
      listHistory = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
        if (this.state.history == '') {
          listHistory = (
            <View
              style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
            >
              <Thumbnail
                square
                large
                source={require("../../../assets/images/empty.png")}
              />
              <Text>No Data!</Text>
            </View>
          );
        } else {
      listHistory = this.state.listHistory.map((listHistory, index) => (
        <View key={index}>
        <View style={{marginTop:2, marginLeft:0, marginRight:0, paddingLeft:10, paddingRight:5, backgroundColor:colors.white}}>
          <View style={{flexDirection:'row', marginBottom:10, marginTop:10}}>
            <View style={{width:'25%', justifyContent: "center"}}>
              <View style={{width:75, height:75}}>
                <Image
                  source={{ uri: GlobalConfig2.SERVERHOST + "images/" + listHistory.image }}
                  style={{ width: '100%', height: '100%', marginTop: 0, marginBottom: 2 }}
                />
              </View>
            </View>
          </View>
        </View>
        </View>
      ))
      }
    }

    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
        <Left style={{ flex: 1 }}>
        </Left>
        <Body style={{flex:3, alignItems:'center' }}>
          <Title style={{fontSize:18, color:colors.black}}>History</Title>
        </Body>
        <Right style={{ flex: 1}}>
          <Text style={{fontSize:12, fontWeight:'bold', color:colors.black}}>Delete All</Text>
        </Right>
      </Header>
      <View style={{ marginTop: 5, flex: 1, paddingRight:5, flexDirection: "column" }}>
        {listHistory}
      </View>
        <CustomFooterStudent navigation={this.props.navigation} menu="History" />
      </Container>
      );
  }
}

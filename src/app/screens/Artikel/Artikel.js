import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
  Form,
  Item,
  Label,
  Input,
  Textarea
} from "native-base";
import styles from "./../styles/Login";
import colors from "../../../styles/colors";
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";

var that;
class ListArtikel extends React.PureComponent {
  render() {
    return (
      <View>
        <ImageBackground
          style={{
            alignSelf: "center",
            width: Dimensions.get("window").width,
            height: 245,
          }}
          source={{ uri: GlobalConfig2.SERVERHOST + "images/Artikel/" + this.props.data.photo }}>
          <TouchableOpacity
            transparent
            style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:320}}
            onPress={()=>that.props.navigation.navigate("HomeMenuStudent")}
          >
            <Icon
              name='arrow-back'
              size={10}
              style={{color:colors.white, fontSize:20}}
            />
          </TouchableOpacity>
        </ImageBackground>
        <View style={{borderTopLeftRadius:30, borderTopRightRadius:30, backgroundColor:colors.white, marginTop:-25, flex:1, alignItems:'center'}}>
          <Text style={{fontSize:16, color:colors.black, fontWeight:'bold', marginTop:15}}>{this.props.data.tittle}</Text>
        </View>
        <View style={{marginTop:10, height:1.2, backgroundColor:colors.primary, marginLeft:20, marginRight:20}}>
        </View>
        <View style={{marginLeft:20, marginRight:20, marginTop:15, flex:1, flexDirection:'row'}}>
          <View style={{width:'5%'}}>
            <Icon
              name='person'
              style={{color:colors.primary, fontSize:15}}
            />
            <Icon
              name='time'
              style={{color:colors.primary, fontSize:15}}
            />
            <Icon
              name='eye'
              style={{color:colors.primary, fontSize:15}}
            />
          </View>
          <View style={{width:'95%'}}>
            <Text style={{fontSize:11}}>{this.props.data.author}</Text>
            <Text style={{fontSize:11}}>{this.props.data.date}</Text>
            <Text style={{fontSize:11}}>350</Text>
          </View>
        </View>
        <View style={{marginLeft:20, marginRight:20, marginTop:15}}>
          <Text style={{fontSize:14, color:colors.gray}}>{this.props.data.text}</Text>
        </View>
      </View>
    );
  }
}


export default class LoginStudent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading : false,
      idArtikel: '',
      listArtikel: [],
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
      AsyncStorage.getItem("idArtikel").then(idArtikel => {
          this.setState({
            idArtikel: idArtikel,
          });
          this.loadArtikel()
      });
  }

  loadArtikel(){
    var url = GlobalConfig.SERVERHOST + 'getAllArtikel';
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',

    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
              this.setState({
                  listArtikel: responseJson.data.filter(x => x.id == this.state.idArtikel),
                  isLoading: false
              });
          }
      })
  }

  _renderItem = ({ item }) => <ListArtikel data={item} />;

  render() {
    that=this;
    return (
      <Container style={styles.wrapper}>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={styles.homeWrapper}>
        <View style={{ flex: 1, flexDirection: "column", backgroundColor: "#fff" }}>
          <ScrollView>
            <FlatList
              data={this.state.listArtikel}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
            />
          </ScrollView>
        </View>
      </View>
      </Container>
      );
  }
}

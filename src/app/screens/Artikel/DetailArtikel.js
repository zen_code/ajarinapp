import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
  Form,
  Item,
  Label,
  Input,
  Textarea
} from "native-base";
import styles from "./../styles/DetailProduk";
import colors from "../../../styles/colors";
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";

export default class DetailArtikel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading : false,
      listArtikel:[],
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
      AsyncStorage.getItem("idArtikel").then(idArtikel => {
          this.setState({
            idArtikel: idArtikel,
          });
          this.loadArtikel()
      });
  }

  loadArtikel(){
    var url = GlobalConfig.SERVERHOST + 'getAllArtikel';
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',

    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
              this.setState({
                  listArtikel: responseJson.data.filter(x => x.id == this.state.idArtikel),
                  isLoading: false
              });
          }
      })
  }



  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={styles.homeWrapper}>
        <View
          style={{ flex: 1, flexDirection: "column", backgroundColor: "#fff" }}
        >
          <View>
            <ScrollView>
              <View style={{paddingLeft:25, paddingRight:25, marginTop:15}}>
                <Text style={{fontSize:50, color:'#315E64', fontWeight:'bold'}}>Hello</Text>
                <Text style={{fontSize:15, color:colors.black}}>Sign In to your account</Text>
              </View>
              <Card style={{ marginLeft: 25, marginRight: 25, borderRadius: 50, marginTop:40, paddingLeft:10, paddingRight:10}}>
                <Textarea style={{fontSize:14, color:colors.gray}} rowSpan={1.8} value={this.state.email} placeholder='Username' onChangeText={(text) => this.setState({ email: text })} />
              </Card>
              <Card style={{ marginLeft: 25, marginRight: 25, borderRadius: 50, marginTop:10, paddingLeft:10, paddingRight:10}}>
                <Textarea style={{fontSize:14, color:colors.gray}} rowSpan={1.8} secureTextEntry={true} value={this.state.password} placeholder='Password' onChangeText={(text) => this.setState({ password: text })} />
              </Card>
              <View style={{paddingLeft:25, paddingRight:25, marginTop:15}}>
                <Text style={{fontSize:12, color:colors.gray, textAlign:'right'}}>Forgot your Password?</Text>
              </View>
              <View style={{marginLeft:100, marginRight:100, marginTop:70}}>
                <Button
                  block
                  style={{
                    width:'100%',
                    height: 40,
                    marginBottom: 20,
                    borderWidth: 0,
                    backgroundColor: colors.primary,
                    borderRadius: 20
                  }}
                  onPress={() => this.konfirmasiLogin()}
                >
                  <Text style={{color:colors.white}}>Sign In</Text>
                </Button>
                <View style={{flex:1, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                  <Text style={{color:colors.gray, fontSize:12, textAlign:'center'}}>Don't have an account? </Text>
                  <TouchableOpacity
                    transparent
                    onPress={()=>this.props.navigation.navigate("RegisterTentor")}
                  >
                  <Text style={{color:colors.black, fontSize:12, fontWeight:'bold'}}>Create</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
        </View>
        </Container>
      );
  }
}

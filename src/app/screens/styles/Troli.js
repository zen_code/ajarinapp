import { StyleSheet } from "react-native";
import iPhoneSize from "../../../helpers/utils";
import colors from "../../../styles/colors";
import { Row } from "native-base";

let labelTextSize = 12;
let weatherTextSize = 18;
let weatherTextSmSize = 14;
let headingTextSize = 28;
if (iPhoneSize() === "small") {
  labelTextSize = 10;
  headingTextSize = 24;
  weatherTextSize = 16;
  weatherTextSmSize = 12;
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    display: "flex",
    backgroundColor:colors.white
  },
  header: {
    backgroundColor: colors.white
  },
  homeWrapper: {
    flex: 1,
    display: "flex"
  },
  searchHeader:{
    width:'100%',
    height:35,
    borderRadius:5,
    marginLeft:10,
    backgroundColor:colors.gray
  },
  iconHeader: {
    color: colors.black,
    fontSize: 20
  },
  kategori: {
    marginTop: 3,
    backgroundColor:colors.white
  },
  itemKategori: {
    flexDirection: "row",
    marginTop: 0,
    marginBottom: 5,
    justifyContent: 'center'
  },
  populer: {
    marginTop: 0,
    backgroundColor:colors.white,
    paddingBottom:5
  },
  itemPopuler: {
    flexDirection: "row",
    marginTop: 5,
    justifyContent: 'center'
  },
  flashSale: {
    marginTop: 0,
    backgroundColor:colors.white
  },
  itemFlashSale: {
    flexDirection: "row",
    marginTop: 0,
    justifyContent: 'center'
  },
  produk: {
    marginTop: 0,
    backgroundColor:colors.gray
  },
  itemProduk: {
    flexDirection: "row",
    marginTop: 0,
    justifyContent: 'center',
  },
  title:{
    flex:1,
    flexDirection:'row',
    marginTop: 3,
    backgroundColor:colors.white,
    paddingTop:5,
    paddingBottom:5,
    paddingLeft:10,
    paddingRight:10,
  },
  title2:{
    flex:1,
    flexDirection:'row',
    marginTop: 0,
    backgroundColor:colors.white,
    paddingTop:5,
    paddingBottom:5,
    paddingLeft:10,
    paddingRight:10,
  },
  titleTextLeft:{
    fontSize:12,
    fontWeight:'bold',
    color:colors.secondary
  },
  titleTextRight:{
    fontSize:12,
    textAlign:'right',
    color:colors.secondary
  },

  btnCheckUot: {
    paddingTop:10,
    paddingBottom:10,
    borderRadius:20,
    width:'100%',
    marginRight:10,
    backgroundColor:colors.secondary,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textCheckOut: {
    fontSize:10,
    fontWeight:'bold',
    color:colors.white
  },
  input: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    padding: 0,
    height: 40,
    width: '80%',
    borderRadius:10,
    borderWidth:1,
    borderColor:colors.graydark,
    backgroundColor: colors.white,
    marginLeft: 10,
    marginBottom: 10,
    marginRight: 3,
    shadowColor: '#3d3d3d',
    shadowRadius: 2,
    shadowOpacity: 0.5,
    shadowOffset: {
    height: 1,
    },
  },
  inputBtn: {
    width: 45,
    flexDirection: 'row',
    alignSelf: 'flex-end',
    padding: 5,
    height: 40,
    borderRadius:10,
    backgroundColor: colors.graydark,
    marginBottom: 10,
    marginRight: 10,
    shadowColor: '#3d3d3d',
    shadowRadius: 2,
    shadowOpacity: 0.5,
    shadowOffset: {
    height: 1,
    },
  },

});

export default styles;

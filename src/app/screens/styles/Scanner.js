import { StyleSheet } from "react-native";
import colors from "../../../styles/colors";

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    display: "flex",
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.gray
  },
  container: {
    display: "flex",
    width:300,
    height:300,
    backgroundColor:colors.white,
    borderRadius:20
  },
  homeWrapper: {
    flex: 1,
    display: "flex"
  },
  title:{
    width:'100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom:50
  }
});

export default styles;

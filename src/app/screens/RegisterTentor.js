import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
  Form,
  Item,
  Label,
  Input,
  Textarea
} from "native-base";
import styles from "./styles/Login";
import colors from "../../styles/colors";
import CheckBox from 'react-native-check-box';
import CustomRadioButton from "../components/CustomRadioButton";
import ImagePicker from "react-native-image-picker";
import GlobalConfig from "../components/GlobalConfig";

export default class RegisterTentor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading : false,
      isAgre:false,
      first_name:'',
      last_name:'',
      email:'',
      phone:'',
      password:'',
      pickedImage: '',
      uri: '',
      fileType:'',
    };
  }

  static navigationOptions = {
    header: null
  };

  pickImageHandler = () => {
    ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
      if (res.didCancel) {
          console.log("User cancelled!");
      } else if (res.error) {
          console.log("Error", res.error);
      } else {
          this.setState({
              pickedImage: res.uri,
              uri: res.uri,
              fileType:res.type
          });
      }
    });
  }

  konfirmasiRegister(){
    if(this.state.first_name==""){
      Alert.alert(
        'Information',
        'Input First Name',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.email==""){
      Alert.alert(
        'Information',
        'Input Email',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.phone==""){
      Alert.alert(
        'Information',
        'Input Phone Number',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.password==""){
      Alert.alert(
        'Information',
        'Input Password',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.uri==""){
      Alert.alert(
        'Information',
        'Upload Your Photo',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.isAgre==false){
      Alert.alert(
        'Information',
        'Agre',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    } else {
          var url = GlobalConfig.SERVERHOST + 'registerTentor';
          var formData = new FormData();
          formData.append("first_name", this.state.first_name)
          formData.append("last_name", this.state.last_name)
          formData.append("email", this.state.email)
          formData.append("phone", this.state.phone)
          formData.append("password", this.state.password)
          formData.append("photo", {
            uri: this.state.uri,
            type: this.state.fileType,
            name: 'imageTentor_' + this.state.first_name
          })
          fetch(url, {
            headers: {
              'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
          }).then((response) => response.json())
            .then((responseJson) => {
              if(responseJson.status == 200) {
                AsyncStorage.setItem('profilStudent', JSON.stringify(responseJson.data)).then(() => {
                  Alert.alert('Success', 'Register Success', [{
                      text: 'OK'
                  }])
                  this.props.navigation.navigate("HomeMenuTentor");
                })
              } else{
                Alert.alert('Error', 'Register Failed', [{
                  text: 'OK'
                }]);
              }
          });
      }
  }


  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={styles.homeWrapper}>
        <View
          style={{ flex: 1, flexDirection: "column", backgroundColor: "#fff", width:'100%' }}
        >
          <View>
            <ScrollView>
              <ImageBackground
                style={{
                  alignSelf: "center",
                  width: Dimensions.get("window").width,
                  height: 135,
                }}
                source={require("../../assets/images/header_tentor_register.png")}>
                <TouchableOpacity
                  transparent
                  style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:320}}
                  onPress={()=>this.props.navigation.navigate("MainMenu")}
                >
                  <Icon
                    name='arrow-back'
                    size={10}
                    style={{color:colors.white, fontSize:20}}
                  />
                </TouchableOpacity>
              </ImageBackground>
              <View style={{marginLeft:25, marginRight:25, flex:1, flexDirection:'row', marginTop:-70}}>
                <View style={{width:'65%'}}>
                </View>
                <View style={{width:'35%', height:105, borderRadius:100, backgroundColor:'#F7F7F8'}}>
                  <Image source={{uri:this.state.pickedImage}} style={styles.previewImage}/>
                </View>
              </View>
              <View style={{marginLeft:25, marginRight:25, flex:1, flexDirection:'row', marginTop:-35}}>
                <View style={{width:'92%'}}>
                </View>
                <View style={{width:'8%', height:26, borderRadius:100, backgroundColor:'#5A5A5A', alignItems:'center', justifyContent:'center'}}>
                  <TouchableOpacity
                    transparent
                    style={{alignItems:'center', justifyContent:'center'}}
                    onPress={this.pickImageHandler}
                    >
                      <Icon
                        name='ios-add'
                        size={10}
                        style={{color:colors.white, fontSize:20, fontWeight:'bold'}}
                      />
                    </TouchableOpacity>
                </View>
              </View>
              <View style={{marginLeft:25, marginRight:25, flex:1, flexDirection:'row', marginTop:-100}}>
                <View style={{width:'65%'}}>
                </View>
                <View style={{width:'35%', height:105, alignItems:'center', justifyContent:'center'}}>
                  {this.state.uri=='' && (
                    <Icon
                      name='person'
                      style={{color:colors.gray, fontSize:60, fontWeight:'bold'}}
                    />
                  )}
                </View>
              </View>
              <Card style={{ marginLeft: 25, marginRight: 25, borderRadius: 50, marginTop:40, paddingLeft:10, paddingRight:10}}>
                <Textarea style={{fontSize:14, color:colors.gray}} rowSpan={1.8} value={this.state.first_name} placeholder='First Name' onChangeText={(text) => this.setState({ first_name: text })} />
              </Card>
              <Card style={{ marginLeft: 25, marginRight: 25, borderRadius: 50, marginTop:20, paddingLeft:10, paddingRight:10}}>
                <Textarea style={{fontSize:14, color:colors.gray}} rowSpan={1.8} value={this.state.last_name} placeholder='Last Name' onChangeText={(text) => this.setState({ last_name: text })} />
              </Card>
              <Card style={{ marginLeft: 25, marginRight: 25, borderRadius: 50, marginTop:20, paddingLeft:10, paddingRight:10}}>
                <Textarea style={{fontSize:14, color:colors.gray}} rowSpan={1.8} value={this.state.email} placeholder='Email Address' onChangeText={(text) => this.setState({ email: text })} />
              </Card>
              <Card style={{ marginLeft: 25, marginRight: 25, borderRadius: 50, marginTop:20, paddingLeft:10, paddingRight:10}}>
                <Textarea style={{fontSize:14, color:colors.gray}} rowSpan={1.8} keyboardType='numeric' value={this.state.phone} placeholder='Mobile Number' onChangeText={(text) => this.setState({ phone: text })} />
              </Card>
              <Card style={{ marginLeft: 25, marginRight: 25, borderRadius: 50, marginTop:20, paddingLeft:10, paddingRight:10}}>
                <Textarea style={{fontSize:14, color:colors.gray}} rowSpan={1.8} secureTextEntry={true} value={this.state.password} placeholder='Password' onChangeText={(text) => this.setState({ password: text })} />
              </Card>
              <View style={{paddingLeft:25, paddingRight:25, marginTop:15}}>
                <CheckBox
                  style={{flex:1, paddingTop:0}}
                  onClick={()=>{
                    this.setState({
                      isAgre:!this.state.isAgre,
                    })
                  }}
                  isChecked={this.state.isAgre}
                  rightText={"I accept all terms and conditions"}
                />
              </View>
              <View style={{marginLeft:100, marginRight:100, marginTop:30}}>
                <Button
                  block
                  style={{
                    width:'100%',
                    height: 40,
                    marginBottom: 20,
                    borderWidth: 0,
                    backgroundColor: colors.primary,
                    borderRadius: 20
                  }}
                  onPress={() => this.konfirmasiRegister()}
                >
                  <Text style={{color:colors.white}}>Sign Up</Text>
                </Button>
              </View>
              <View style={{flex:1, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                <Text style={{color:colors.gray, fontSize:12, textAlign:'center'}}>Or Create account using social media</Text>
              </View>
              <View style={{flex:1, flexDirection:'row', alignItems:'center', justifyContent:'center', marginTop:5}}>
                <View style={{backgroundColor:'#E1543E', width:30, height:30, borderRadius:40, alignItems:'center', justifyContent:'center'}}>
                  <Icon
                    name='logo-googleplus'
                    style={{color:colors.white, fontSize:20, marginTop:0}}
                  />
                </View>
                <View style={{backgroundColor:'#5CAAE1', width:30, height:30, borderRadius:40, marginLeft:10, marginRight:10, alignItems:'center', justifyContent:'center'}}>
                  <Icon
                    name='logo-twitter'
                    style={{color:colors.white, fontSize:20, marginTop:0}}
                  />
                </View>
                <View style={{backgroundColor:'#355BA4', width:30, height:30, borderRadius:40, alignItems:'center', justifyContent:'center'}}>
                  <Icon
                    name='logo-facebook'
                    style={{color:colors.white, fontSize:20, marginTop:0}}
                  />
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
        </View>
        </Container>
      );
  }
}

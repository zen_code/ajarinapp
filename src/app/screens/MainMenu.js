import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
  Form,
  Item,
  Label,
  Input,
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import styles from "./styles/Login";
import colors from "../../styles/colors";
import GlobalConfig from "../components/GlobalConfig";

export default class MainMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading : false,
      login:false,
      register:false,
    };
  }

  static navigationOptions = {
    header: null
  };

  login(){
    this.setState({
      login: true
    });
  }

  loginStudent(){
    this.props.navigation.navigate('LoginStudent');
    this.setState({
      login: false
    });
  }

  loginTentor(){
    this.props.navigation.navigate('LoginTentor');
    this.setState({
      login: false
    });
  }

  register(){
    this.setState({
      register: true
    });
  }

  registerStudent(){
    this.props.navigation.navigate('RegisterStudent');
    this.setState({
      register: false
    });
  }

  registerTentor(){
    this.props.navigation.navigate('RegisterTentor');
    this.setState({
      register: false
    });
  }

  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={styles.homeWrapper}>
        <View
          style={{ flex: 1, flexDirection: "column", backgroundColor: "#fff" }}
        >
          <View>
            <ScrollView>
              <ImageBackground
                style={{
                  alignSelf: "center",
                  width: Dimensions.get("window").width,
                  height: Dimensions.get("window").height,
                }}
                source={require("../../assets/images/background.png")}>
                <TouchableOpacity
                  transparent
                  style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:320}}
                  onPress={()=>this.logOut()}
                >
                </TouchableOpacity>
              </ImageBackground>
              <View style={{ flex: 1, flexDirection:'column', marginTop:-140, marginLeft:20, marginRight:20}}>
                <View>
                  <Button
                    block
                    style={{
                      width:'100%',
                      height: 40,
                      marginBottom: 10,
                      borderWidth: 0,
                      backgroundColor: colors.secondary,
                      borderRadius: 15
                    }}
                    onPress={() => this.login()}
                  >
                    <Text style={{color:colors.white}}>Login</Text>
                  </Button>
                </View>
                <View>
                  <Button
                    block
                    style={{
                      width:'100%',
                      height: 40,
                      marginBottom: 25,
                      borderWidth: 0,
                      backgroundColor: colors.primary,
                      borderRadius: 15
                    }}
                    onPress={() => this.register()}
                  >
                    <Text style={{color:colors.white}}>Register</Text>
                  </Button>
                </View>
              </View>
            </ScrollView>
            <View style={{ width: '100%', position: "absolute"}}>
                <Dialog
                  visible={this.state.login}
                  dialogAnimation={
                    new SlideAnimation({
                      slideFrom: "bottom"
                    })
                  }
                  dialogStyle={{ position: "absolute", top: this.state.posDialog, width:300}}
                  onTouchOutside={() => {
                    this.setState({ login: false });
                  }}
                >
                  <DialogContent>
                    {
                    <View>
                      <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                        <Text style={{fontSize:18, alignItems:'center', color:colors.black}}>Login</Text>
                      </View>
                      <View style={{flexDirection:'row', flex:1, paddingTop:10, width:'100%'}}>
                        <View style={{width:'50%', paddingRight:5}}>
                          <Button
                            block
                            style={{
                              width:'100%',
                              marginTop:10,
                              height: 35,
                              marginBottom: 5,
                              borderWidth: 0,
                              backgroundColor: colors.primary,
                              borderRadius: 15
                            }}
                            onPress={() => this.loginStudent()}
                          >
                            <Text style={{color:colors.white}}>Student</Text>
                          </Button>
                        </View>
                        <View style={{width:'50%', paddingLeft:5}}>
                          <Button
                            block
                            style={{
                              width:'100%',
                              marginTop:10,
                              height: 35,
                              marginBottom: 5,
                              borderWidth: 0,
                              backgroundColor: colors.primary,
                              borderRadius: 15
                            }}
                            onPress={() => this.loginTentor()}
                          >
                            <Text style={{color:colors.white}}>Tentor</Text>
                          </Button>
                        </View>
                      </View>
                    </View>
                    }
                  </DialogContent>
                </Dialog>
              </View>

              <View style={{ width: '100%', position: "absolute"}}>
                  <Dialog
                    visible={this.state.register}
                    dialogAnimation={
                      new SlideAnimation({
                        slideFrom: "bottom"
                      })
                    }
                    dialogStyle={{ position: "absolute", top: this.state.posDialog, width:300}}
                    onTouchOutside={() => {
                      this.setState({ register: false });
                    }}
                  >
                    <DialogContent>
                      {
                      <View>
                        <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                          <Text style={{fontSize:18, alignItems:'center', color:colors.black}}>Register</Text>
                        </View>
                        <View style={{flexDirection:'row', flex:1, paddingTop:10, width:'100%'}}>
                          <View style={{width:'50%', paddingRight:5}}>
                            <Button
                              block
                              style={{
                                width:'100%',
                                marginTop:10,
                                height: 35,
                                marginBottom: 5,
                                borderWidth: 0,
                                backgroundColor: colors.primary,
                                borderRadius: 15
                              }}
                              onPress={() => this.registerStudent()}
                            >
                              <Text style={{color:colors.white}}>Student</Text>
                            </Button>
                          </View>
                          <View style={{width:'50%', paddingLeft:5}}>
                            <Button
                              block
                              style={{
                                width:'100%',
                                marginTop:10,
                                height: 35,
                                marginBottom: 5,
                                borderWidth: 0,
                                backgroundColor: colors.primary,
                                borderRadius: 15
                              }}
                              onPress={() => this.registerTentor()}
                            >
                              <Text style={{color:colors.white}}>Tentor</Text>
                            </Button>
                          </View>
                        </View>
                      </View>
                      }
                    </DialogContent>
                  </Dialog>
                </View>

          </View>
        </View>
        </View>
        </Container>
      );
  }
}

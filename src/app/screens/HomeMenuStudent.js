import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
  Textarea
} from "native-base";
import styles from "./styles/HomeMenu";
import colors from "../../styles/colors";
import CustomFooterStudent from "../components/CustomFooterStudent";
import Slideshow from "react-native-slideshow";
import PropTypes from 'prop-types';
import SubProduk from "../components/SubProduk";
import SubArtikel from "../components/SubArtikel";
import SubNewTentor from "../components/SubNewTentor";
import Ripple from "react-native-material-ripple";
import loadProduk from "../data/allProduk.json";
import GlobalConfig from "../components/GlobalConfig";
import GlobalConfig2 from "../components/GlobalConfig2";
import GlobalConfig3 from "../components/GlobalConfig3";
import Moment from 'moment'

var that;
class ListArtikel extends React.PureComponent {
  navigateToScreen(route, idArtikel) {
    AsyncStorage.setItem("idArtikel", JSON.stringify(idArtikel)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={{width:300, paddingRight:5}}>
      <TouchableOpacity
        transparent
        onPress={() => this.navigateToScreen("Artikel", this.props.data.id)}>
          <SubArtikel
            imageUri={{ uri: GlobalConfig2.SERVERHOST + "images/Artikel/" + this.props.data.photo }}
            tittle={this.props.data.tittle}
            text_header={this.props.data.text_header}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

class ListNewTentor extends React.PureComponent {
  navigateToScreen(route, dataTentor) {
    AsyncStorage.setItem("dataTentor", JSON.stringify(dataTentor)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={{width:130, paddingRight:5}}>
      <TouchableOpacity
        transparent
        onPress={() => this.navigateToScreen("DetailTentor", this.props.data)}>
          <SubNewTentor
            imageUri={{ uri: GlobalConfig2.SERVERHOST + "images/" + this.props.data.photo }}
            name={this.props.data.name}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

class ListItem extends React.PureComponent {
  navigateToScreen(route, listProduk) {
    AsyncStorage.setItem("listProduk", JSON.stringify(listProduk)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={{width:'50%', paddingRight:5}}>
      <TouchableOpacity
        transparent
        onPress={() => this.navigateToScreen("DetailProduk", this.props.data)}>
        <SubProduk
          imageUri={{ uri: GlobalConfig2.SERVERHOST + "images/" + this.props.data.image_primary }}
          name={this.props.data.name}
          harga={this.props.data.harga}
          terjual={this.props.data.stok}
          favorite={this.props.data.berat}
        />
        </TouchableOpacity>
      </View>
    );
  }
}

export default class HomeMenuStudent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: false,
      search:"Search",
      listArtikel:[],
      listTentor:[],
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    AsyncStorage.getItem("profilStudent").then(profilStudent => {
      //alert(JSON.stringify(profilStudent))
    });
    this.loadArtikel()
    this.loadTentor()
    // this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
    //   this.loadProduk()
    //   this.loadShop()
    // });
  }

  loadArtikel(){
    var url = GlobalConfig.SERVERHOST + 'getAllArtikel';
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',

    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
              this.setState({
                  listArtikel: responseJson.data,
                  isLoading: false
              });
          }
      })
  }

  loadTentor(){
    var url = GlobalConfig.SERVERHOST + 'getAllDetailTentor';
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',

    }).then((response) => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
              this.setState({
                  listTentor: responseJson.data,
                  isLoading: false
              });
          }
      })
  }

  navigateToScreen(route, filter) {
    AsyncStorage.setItem("filter", (JSON.stringify(filter))).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  _renderItem = ({ item }) => <ListItem data={item} />;
  _renderArtikel = ({ item }) => <ListArtikel data={item} />;
  _renderNewTentor = ({ item }) => <ListNewTentor data={item} />;

  render() {
    that=this;
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <ScrollView>
        <ImageBackground
          style={{
            alignSelf: "center",
            width: Dimensions.get("window").width,
            height: 200,
          }}
          source={require("../../assets/images/header_home.png")}>
          <TouchableOpacity
            transparent
            style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:320}}
            onPress={()=>this.logOut()}
          >
          </TouchableOpacity>
        </ImageBackground>
        <Card style={{ marginLeft: 25, marginRight: 25, borderRadius: 50, marginTop:-25, paddingLeft:10, paddingRight:10, flex:1, flexDirection:'row'}}>
          <View style={{width:'5%'}}>
            <Icon
              name='search'
              style={{color:colors.gray, fontSize:22, paddingTop:13}}
            />
          </View>
          <View style={{width:'88%', paddingTop:4}}>
            <Textarea style={{fontSize:14, color:colors.gray}} rowSpan={1.8} value={this.state.username} placeholder='Search Tentor' onChangeText={(text) => this.setState({ username: text })} />
          </View>
          <View style={{width:'7%'}}>
            <Icon
              name='qr-scanner'
              style={{color:colors.gray, fontSize:22, paddingTop:13}}
            />
          </View>
        </Card>
          <View style={styles.title}>
            <View style={{width:'50%', paddingLeft:15}}>
              <Text style={styles.titleTextLeft}>Artikel</Text>
            </View>
            <View style={{width:'50%', paddingRight:15}}>
              <TouchableOpacity
                transparent
                onPress={() => this.props.navigation.navigate("AllArtikel")}
              >
              <Text style={styles.titleTextRight}>View All></Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{width:'100%', marginTop:0, paddingBottom:5, backgroundColor:colors.white, paddingLeft:15, paddingRight:15}}>
            <View style={{marginTop:0}}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <FlatList
                horizontal={true}
                data={this.state.listArtikel}
                renderItem={this._renderArtikel}
                keyExtractor={(item, index) => index.toString()}
              />
            </ScrollView>
            </View>
          </View>

          <View style={styles.title}>
            <View style={{width:'50%',paddingLeft:15}}>
              <Text style={styles.titleTextLeft}>Tentor</Text>
            </View>
            <View style={{width:'50%', paddingRight:15}}>
              <TouchableOpacity
                transparent
                onPress={() => this.props.navigation.navigate("AllTentor")}
              >
              <Text style={styles.titleTextRight}>View All></Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{width:'100%', marginTop:0, paddingBottom:5, backgroundColor:colors.white, paddingLeft:15, paddingRight:15}}>
            <View style={{marginTop:0}}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <FlatList
                horizontal={true}
                data={this.state.listTentor}
                renderItem={this._renderNewTentor}
                keyExtractor={(item, index) => index.toString()}
              />
            </ScrollView>
            </View>
          </View>

          {/*<View style={styles.title}>
            <View style={{width:'50%', paddingLeft:15}}>
              <Text style={styles.titleTextLeft}>Popular Tentor</Text>
            </View>
            <View style={{width:'50%', paddingRight:15}}>
              <TouchableOpacity
                transparent
                onPress={() => this.props.navigation.navigate("AllProduk")}
              >
              <Text style={styles.titleTextRight}>View All></Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{width:'100%', marginTop:5, paddingRight:5, paddingLeft:15, paddingRight:15}}>
            <View style={{marginTop:0}}>
              <FlatList
                data={this.state.listProduk}
                renderItem={this._renderItem}
                numColumns={2}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
          </View>*/}
      </ScrollView>
      <CustomFooterStudent navigation={this.props.navigation} menu="Home" />

      </Container>
      );
  }
}

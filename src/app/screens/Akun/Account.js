import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  BackHandler,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import styles from "./../styles/Account";
import colors from "../../../styles/colors";
import CustomFooterStudent from "../../components/CustomFooterStudent";
import Ripple from "react-native-material-ripple";
import GlobalConfig from "../../components/GlobalConfig";
import GlobalConfig2 from "../../components/GlobalConfig2";

export default class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: false,
      dataUser:[],
      username:'',
      visibleRating: false,
      visibleVoucher: false,
      visibleHelp: false,
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.backPressed);
    AsyncStorage.getItem("profilStudent").then(profilStudent => {
        this.setState({
          first_name: JSON.parse(profilStudent).first_name,
          address: JSON.parse(profilStudent).address,
          email: JSON.parse(profilStudent).email,
          photo: JSON.parse(profilStudent).photo,
        });
    });
    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
  }

  backPressed = () => {
      this.props.navigation.goBack(null);
      return true;
  };

  exitApp(){
    Alert.alert(
      'Confirmation',
      'Logout Tentor.id ?',
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "Yes", onPress: () => BackHandler.exitApp() }
      ],
      { cancelable: false }
    );
  }

  loadRating(){
    this.setState({
      visibleRating: true
    });
  }

  loadVoucher(){
    this.setState({
      visibleVoucher: true
    });
  }

  loadHelp(){
    this.setState({
      visibleHelp: true
    });
  }

  topup(){
    Alert.alert(
      'Information',
      'Under Construction',
      [
        { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
      ],
      { cancelable: false }
    );
  }

  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <ScrollView>
        <Content style={{ marginTop:0}}>
          <View style={{width: '100%', height:150}}>
            <Image
              style={{
                alignSelf: "center",
                width: '100%',
                height: '100%',
              }}
              source={require("../../../assets/images/Account.png")}>
            </Image>
          </View>
          <View style={{width:'100%', paddingLeft:10, paddingRight:10, marginTop:20}}>
            <View style={{borderRadius:10, borderWidth:1, borderColor:colors.graydark, flex:1, flexDirection:'row'}}>
              <View style={{width:'65%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5}}>
                <View style={{width:'20%'}}>
                    <Image
                      style={{
                        height: 50,
                        width: 50,
                        borderRadius: 50
                      }}
                      source={{
                        uri:
                        GlobalConfig2.SERVERHOST + "images/" + this.state.photo}}/>
                </View>
                <View style={{width:'90%'}}>
                  <Text style={{fontSize:12, paddingTop:0, fontWeight:'bold', color:colors.black}}>{this.state.first_name}</Text>
                  <View style={{flex:1, flexDirection:'row'}}>
                    <Icon
                        name='ios-pin'
                        style={{fontSize:15, color:colors.primary, paddingRight:5}}
                    />
                      <Text style={{fontSize:12, paddingTop:0}}>{this.state.address}</Text>
                  </View>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <Icon
                          name='mail-open'
                          style={{fontSize:15, color:colors.primary, paddingRight:5}}
                      />
                    <Text style={{fontSize:12, paddingTop:0}}>{this.state.email}</Text>
                  </View>
                </View>
              </View>

              <View style={{width:'20%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <View style={{width:'100%', height:30, marginRight:10, justifyContent: "center", alignItems: "center", backgroundColor:colors.secondary, borderRadius:30}}>
                <TouchableOpacity
                  transparent
                  onPress={() =>this.topup()
                    }
                >
                  <Text style={{fontSize:12, color:colors.white}}>Top Up</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
          <TouchableOpacity
            transparent
            onPress={() => this.loadRating()}
          >
          <View style={{width:'100%', paddingLeft:10, paddingRight:10, marginTop:10}}>
            <View style={{flex:1, flexDirection:'row', paddingTop:5, paddingBottom:5}}>
              <View style={{width:'10%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <Icon
                    name='ios-star'
                    style={{fontSize:25, color:'#EB9201'}}
                />
              </View>
              <View style={{width:'80%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center"}}>
                <Text style={{fontSize:12}}>My Rating</Text>
              </View>
              <View style={{width:'10%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <Icon
                    name='ios-play'
                    style={{fontSize:20, color:colors.graydark}}
                />
              </View>
            </View>
          </View>
          </TouchableOpacity>
          <TouchableOpacity
            transparent
            onPress={() => this.loadVoucher()}
          >
          <View style={{width:'100%', paddingLeft:10, paddingRight:10, marginTop:2}}>
            <View style={{flex:1, flexDirection:'row', paddingTop:5, paddingBottom:5}}>
              <View style={{width:'10%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <Icon
                    name='logo-usd'
                    style={{fontSize:23, color:'#3C9EF6'}}
                />
              </View>
              <View style={{width:'80%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center"}}>
                <Text style={{fontSize:12}}>My Vouchers</Text>
              </View>
              <View style={{width:'10%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <Icon
                    name='ios-play'
                    style={{fontSize:20, color:colors.graydark}}
                />
              </View>
            </View>
          </View>
          </TouchableOpacity>
          <TouchableOpacity
            transparent
            onPress={() => this.loadHelp()}
          >
          <View style={{width:'100%', paddingLeft:10, paddingRight:10, marginTop:2}}>
            <View style={{ flex:1, flexDirection:'row', paddingTop:5, paddingBottom:5}}>
              <View style={{width:'10%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <Icon
                    name='help-circle'
                    style={{fontSize:25, color:'#FF5E2F'}}
                />
              </View>
              <View style={{width:'80%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center"}}>
                <Text style={{fontSize:12}}>Help Centre</Text>
              </View>
              <View style={{width:'10%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <Icon
                    name='ios-play'
                    style={{fontSize:20, color:colors.graydark}}
                />
              </View>
            </View>
          </View>
          </TouchableOpacity>
          <TouchableOpacity
            transparent
            onPress={() => this.exitApp()}
          >
          <View style={{width:'100%', paddingLeft:10, paddingRight:10, marginTop:2}}>
            <View style={{flex:1, flexDirection:'row', paddingTop:5, paddingBottom:5}}>
              <View style={{width:'10%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <Icon
                    name='log-out'
                    style={{fontSize:25, color:'#319F4C'}}
                />
              </View>
              <View style={{width:'80%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center"}}>
                <Text style={{fontSize:12}}>Logout</Text>
              </View>
              <View style={{width:'10%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <Icon
                    name='ios-play'
                    style={{fontSize:20, color:colors.graydark}}
                />
              </View>
            </View>
          </View>
          </TouchableOpacity>
        </Content>
      </ScrollView>

      <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleRating}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog }}
            onTouchOutside={() => {
              this.setState({ visibleRating: false });
            }}
            dialogTitle={<DialogTitle title="My Rating" />}
            actions={[
              <DialogButton
                style={{
                  fontSize: 11,
                  backgroundColor: colors.white,
                  borderColor: colors.blue01
                }}
                text="SORT"
                onPress={() => this.loadData()}
                // onPress={() => this.setState({ visibleFilter: false })}
              />
            ]}
          >
            <DialogContent>
              {
                <View style={{flexDirection:'row', flex:1, paddingTop:5}}>
                  <View style={{paddingRight:5}}>
                    <Icon
                        name='ios-star'
                        style={{fontSize:35, color:'#EB9201'}}
                    />
                  </View>
                  <View style={{paddingRight:5}}>
                    <Icon
                        name='ios-star'
                        style={{fontSize:35, color:'#EB9201'}}
                    />
                  </View>
                  <View style={{paddingRight:5}}>
                    <Icon
                        name='ios-star'
                        style={{fontSize:35, color:'#EB9201'}}
                    />
                  </View>
                  <View style={{paddingRight:5}}>
                    <Icon
                        name='ios-star'
                        style={{fontSize:35, color:'#EB9201'}}
                    />
                  </View>
                  <View style={{paddingRight:5}}>
                    <Icon
                        name='ios-star'
                        style={{fontSize:35, color:'#DEDFDF'}}
                    />
                  </View>
                </View>
              }
            </DialogContent>
          </Dialog>
        </View>

      <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleVoucher}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog }}
            onTouchOutside={() => {
              this.setState({ visibleVoucher: false });
            }}
            dialogTitle={<DialogTitle title="My Vouchers" />}
            actions={[
              <DialogButton
                style={{
                  fontSize: 11,
                  backgroundColor: colors.white,
                  borderColor: colors.blue01
                }}
                text="SORT"
                onPress={() => this.loadData()}
                // onPress={() => this.setState({ visibleFilter: false })}
              />
            ]}
          >
            <DialogContent>
              {
                <View style={{flexDirection:'row', flex:1, paddingTop:5, alignItems:'center'}}>
                    <Text style={{fontSize:30, fontWeight:'bold', textAlign:'center'}}>RP. 0,-</Text>
                </View>
              }
            </DialogContent>
          </Dialog>
        </View>

        <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleHelp}
              dialogAnimation={
                new SlideAnimation({
                  slideFrom: "bottom"
                })
              }
              dialogStyle={{ position: "absolute", top: this.state.posDialog }}
              onTouchOutside={() => {
                this.setState({ visibleHelp: false });
              }}
              dialogTitle={<DialogTitle title="Help Centre" />}
              actions={[
                <DialogButton
                  style={{
                    fontSize: 11,
                    backgroundColor: colors.white,
                    borderColor: colors.blue01
                  }}
                  text="SORT"
                  onPress={() => this.loadData()}
                  // onPress={() => this.setState({ visibleFilter: false })}
                />
              ]}
            >
              <DialogContent>
                {
                  <View style={{flexDirection:'row', flex:1, paddingTop:5, alignItems:'center'}}>
                      <Text style={{fontSize:20, fontWeight:'bold', textAlign:'center'}}>official@tentor.id</Text>
                  </View>
                }
              </DialogContent>
            </Dialog>
          </View>
      <CustomFooterStudent navigation={this.props.navigation} menu="Account" />
      </Container>
      );
  }
}

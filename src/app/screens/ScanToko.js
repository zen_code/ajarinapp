import React, { Component } from 'react';
import {
  Alert,
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TouchableOpacity,
  Linking,
  AsyncStorage,
  FlatList,
  LayoutAnimation,
  BackHandler
} from 'react-native';

import {
  Button,
  Form,
  Item,
  Label,
  Input,
  Content,
  Thumbnail
} from 'native-base';

import { StackNavigator, NavigationActions } from 'react-navigation';
import colors from "../../styles/colors";
import styles from "./styles/Scanner";
import QRCodeScanner from 'react-native-qrcode-scanner';
import GlobalConfig from "../components/GlobalConfig";

class ScanToko extends Component {
  static navigationOptions = {
        header: null,
  }

  constructor(props) {
    super(props);
    this.state = {
      lastScannedUrl: [],
      dataQR: [],
    };
  }



  componentDidMount() {

  }

  navigateToScreen(route, idToko) {
    AsyncStorage.setItem("idToko", idToko).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  _handleQRCodeRead = result => {
    var qrCode = result.data
    var validasi = qrCode.substring(0, 6)
    if(validasi!='KAYUKU') {
      Alert.alert(
        'Information',
        'Shop Not Found',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
      this.props.navigation.navigate("HomeMenu")
    } else {
        var qr = qrCode.replace("KAYUKU~", "")
        LayoutAnimation.spring();
        var url = GlobalConfig.SERVERHOST + 'get_toko_all';
        fetch(url, {
          headers: {
            'Content-Type': 'multipart/form-data'
          },
          method: 'GET',
        }).then((response) => response.json())
          .then((responseJson) => {
              if(JSON.stringify(responseJson.data.filter(x => x.id == qr)) !== '[]'){
                Alert.alert(
                  'Information',
                  'Shop Found',
                  [
                    { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                  ],
                  { cancelable: false }
                );
                this.navigateToScreen("DetailShop", qr)
              } else {
                Alert.alert(
                  'Information',
                  'Shop Not Found',
                  [
                    { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                  ],
                  { cancelable: false }
                );
                this.props.navigation.navigate("HomeMenu")
              }

          })
          .catch((error) => {
              console.log(error)
          })
    }
}

  render() {
    return (
      <View style={styles.wrapper}>
        <View style={styles.title}>
          <Text style={{fontWeight:'bold', fontSize:20}}>Scan QR Code</Text>
        </View>
        <View style={styles.container}>
          <QRCodeScanner onRead={this._handleQRCodeRead}/>
        </View>
        <View style={{width:'100%', paddingTop:150}}>
          <Button
              block
              style={{
                height: 45,
                marginLeft: 20,
                marginRight: 20,
                marginBottom: 20,
                backgroundColor: colors.secondary,
                borderRadius: 4
              }}
              onPress={() => this.props.navigation.navigate("HomeMenu")}
            >
              <Text style={{fontSize: 14, fontWeight: 'bold', color: colors.white}}>Kembali</Text>
          </Button>
        </View>
      </View>
    );
  }
}

export default ScanToko;

import React, { Component } from "react";
import { View, Text, Image, TouchableNativeFeedback } from "react-native";
import { Icon } from "native-base";
import colors from "../../styles/colors";
import PropTypes from "prop-types";

let styles = {
  container: {
    flex: 1,
    display: "flex",
    flexDirection:'row',
    height: '100%',
    width: '100%',
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 0,
    paddingTop:5,
    paddingBottom:5,
    borderWidth:1,
    borderRadius:10,
    borderColor:colors.gray,
  },
  b: {
    backgroundColor: colors.primary
  }
};

export default class SubPopuler extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { loading, disabled, handleOnPress } = this.props;
    return (
      <View style={[styles.container]}>
        <View style={{width:'40%', height:60, marginLeft:5 }}>
          <Image
            source={this.props.imageUri}
            style={{ width: '100%', height: '100%', marginTop: 0, marginBottom: 2 }}
          />
        </View>
        <View style={{ flex: 1, width:'60%', flexDirection: 'column', paddingTop: 3, marginLeft:5}}>
            <Text style={{fontSize:11, color:colors.black, fontWeight: "bold",}}>{this.props.name}</Text>
            <Text style={{fontSize:10, fontWeight: "bold",}}>{this.props.jumlah}</Text>
        </View>
      </View>
    );
  }
}


SubPopuler.propTypes = {
  handleOnPress: PropTypes.func,
  disabled: PropTypes.bool
};

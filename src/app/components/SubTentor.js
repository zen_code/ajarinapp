import React, { Component } from "react";
import { View, Text, Image, TouchableNativeFeedback } from "react-native";
import { Icon } from "native-base";
import colors from "../../styles/colors";
import PropTypes from "prop-types";

let styles = {
  container: {
    flex: 1,
    height: 85,
    width: '100%',
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 0,
    borderRadius:10,
    marginTop:5,
    backgroundColor:colors.white,
  },
  b: {
    backgroundColor: colors.primary
  }
};

export default class SubTentor extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { loading, disabled, handleOnPress } = this.props;
    return (
      <View style={[styles.container]}>
      <View style={{flexDirection:'row', marginBottom:10, marginTop:10}}>
        <View style={{width:'25%', justifyContent: "center", paddingLeft:5}}>
          <View style={{width:70, height:70, borderRadius:40}}>
            <Image
              source={this.props.imageUri}
              style={{ width: '100%', height: '100%', marginTop: 0, marginBottom: 2, borderRadius:40,}}
            />
          </View>
        </View>
        <View style={{width:'55%'}}>
          <Text style={{fontSize:12, color:colors.black, fontWeight:'bold'}}>{this.props.name}</Text>
          <Text style={{fontSize:10, color:colors.gray}}>{this.props.kampus}</Text>
          <View style={{flex:1, flexDirection:'row'}}>
              <Icon
                  name='ios-pin'
                  style={{fontSize:15, color:colors.primary, paddingRight:5}}
              />
            <Text style={{fontSize:12, paddingTop:0}}>{this.props.address}</Text>
          </View>
          <View style={{flex:1, flexDirection:'row'}}>
              <Icon
                  name='book'
                  style={{fontSize:15, color:colors.primary, paddingRight:5}}
              />
            <Text style={{fontSize:12, paddingTop:0}}>{this.props.mapel}</Text>
          </View>
        </View>
        <View style={{width:'20%'}}>
        <View style={{flexDirection:'row', flex:1, paddingTop:4}}>
          <View style={{paddingRight:2}}>
            <Icon
                name='ios-star'
                style={{fontSize:10, color:'#EB9201'}}
            />
          </View>
          <View style={{paddingRight:2}}>
            <Icon
                name='ios-star'
                style={{fontSize:10, color:'#EB9201'}}
            />
          </View>
          <View style={{paddingRight:2}}>
            <Icon
                name='ios-star'
                style={{fontSize:10, color:'#EB9201'}}
            />
          </View>
          <View style={{paddingRight:2}}>
            <Icon
                name='ios-star'
                style={{fontSize:10, color:'#EB9201'}}
            />
          </View>
          <View style={{paddingRight:2}}>
            <Icon
                name='ios-star'
                style={{fontSize:10, color:'#DEDFDF'}}
            />
          </View>
        </View>
        </View>
      </View>
      </View>
    );
  }
}


SubTentor.propTypes = {
  handleOnPress: PropTypes.func,
  disabled: PropTypes.bool
};

import React, { Component } from "react";
import { Platform, Image, TouchableOpacity } from "react-native";
import { Footer, FooterTab, Text, Button, Icon, View } from "native-base";
import colors from "../../styles/colors";

class CustomRadioButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      statRadio: []
    };
  }


  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={{flexDirection: "row", marginTop:10}}>
          <View
            style={{
              height: 20,
              width: 20,
              borderRadius: 12,
              borderWidth: 2,
              borderColor: colors.secondary,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            {this.props.selected ? (
              <View
                style={{
                  height: 10,
                  width: 10,
                  borderRadius: 6,
                  backgroundColor: colors.secondary
                }}
              />
            ) : null}
          </View>
          <Text style={{fontSize:12}}> {this.props.name}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default CustomRadioButton;

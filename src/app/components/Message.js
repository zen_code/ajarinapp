import React from 'react';
import { View, StyleSheet, Text } from 'react-native'
import colors from '../../styles/colors';

const Message = ({ item }) => (
  <View style={[
      styles.message, item.incoming &&
      styles.incomingMessage
    ]}>
    <Text>{item.message}</Text>
  </View>
)

const styles = {
  message: {
    width: '70%',
    margin: 10,
    padding: 10,
    backgroundColor: colors.gray,
    borderRadius: 10
  },
  incomingMessage: {
    alignSelf: 'flex-end',
    backgroundColor: colors.graydark
  }
}

export default Message;

export default {
  primary : "#3DA1D5",
  secondary : "#41A6C1",
  gray: "#A8A8A8",

  primarydark : "#663300",
  white: "#ffffff",
  black: "#373435",
  green: "#00b300",
  lightBlack: "#484848",

  graydark: "#DEDFDF",
  grayprimary: "#F2F2F2",
  red: "#FF0000",
  accent: "#E74424",
};

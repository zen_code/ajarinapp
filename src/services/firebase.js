import * as firebase from "firebase";

export const initialize = () => firebase.initializeApp({
    apiKey: "AIzaSyCu5sr5nbzldzPGxFUMI-0uSYluYGtEcKo",
    authDomain: "kayuapp-kayuapp.firebaseapp.com",
    databaseURL: "https://kayuapp-kayuapp.firebaseio.com",
    projectId: "kayuapp-kayuapp",
    storageBucket: "kayuapp-kayuapp.appspot.com",
    messagingSenderId: "910531790660"
});

export const setListener = (endpoint, updaterFn) => {
	firebase.database().ref(endpoint).on('value', updaterFn);
	return () => firebase.database().ref(endpoint).off();
}

export const pushData = (endpoint, data) => {
	return firebase.database().ref(endpoint).push(data);
}
